# SimpleErrorHandler

- Wrapper for `bugsnag\bugsnag`.  
- Designed to be the base class to catch and notify errors in a standard way.  
- Very useful for chained class operations.  
- Can be casted to string, if `hasError`, will show error details.  

---  
## Namespace  

`SimplePHP\SimpleServer\SimpleErrorHandler`  

---  
## Environment variables  

Define environment variables using `putenv(VAR=VALUE)`.  

Variable | Required | Description  
-- | -- | --  
SIMPLE_PHP_BUGSNAG | Yes | The Bugsnag key, required if using Bugsnag.  
SIMPLE_PHP_VERSION | No | App version number. (default = 1.0.0)
SIMPLE_PHP_ENV_MODE | No | `development` or `production` (default). Development will provide stack trace information.  

---  
## Usage: Handle global unhandled errors  

````php
// index.php 
// note: registerBugsnagUnhandled is not a static method 
(new SimpleErrorHandler())->registerBugsnagUnhandled();
````

---  
## Usage: Implemented by another class  

````php  
Class A extends SimpleErrorHandler {

  public $value = 'default';

  public function __construct() {
    SimpleErrorHandler::__construct();
  }

  public function risky() {
    try {
      $this->value = 'risky';

      // some risky code 
      throw new \Exception('risky operation');
      
    } catch (\Exception $e) {
      // leave a breadcrumb in Bugsnag (optional)
      $this->breadcrumb('we failed');

      // sets $this->hasFault to the GeneralFault object, 
      // which contains information about the fault 
      $this->stop($e);
    } finally {
      // chain
      return $this;
    }
  }

  public function safe() {
    return $this;
  }

  public function chained() {
    // skip if we are in fault 
    if ($this->hasFault) return;

    $this->value = 'success';

    // chain
    return $this;
  }
}
````

**Test risky operation**
````php
$test = new A();
$test->risky()->chained();

echo $test->value; 
// risky

echo nl2br($test);
// Class SimpleErrorHandler
// 500: risky operation
// Class: Exception
// File: /path/to/file
// Line: 14
````

**Test safe operation**
````php
$test = new A();
$test->safe()->chained();

echo $test->value; 
// success

echo nl2br($test);
// Class SimpleErrorHandler
````

---  
## Usage: Replace default error handler 

This is to convert all notices, warnings and exception error ErrorException 
for more reliable catching.  

```php
// index.php 
// this is a static function
set_error_handler(
  'SimplePHP\SimpleServer\SimpleErrorHandler::errorHandler', E_ALL
);
```

---  
## License  
MIT.  
Johnson Zhou [johnson@simplyuseful.io](mailto://johnson@simplyuseful.io).