# SimpleRouter  

- Chainable wrapper for AltoRouter.  
- Uses SimpleErrorHandler.  
- Example implementation [index.php](https://bitbucket.org/johnsonjzhou/simple-php/src/master/template/src/index.php)  

---  
## Namespace  

`SimplePHP\SimpleServer\SimpleRouter`  

---  
## Usage  

````php
use SimplePHP\SimpleServer\SimpleRouter;

(new SimpleRouter('path/to/basepath')) // basepath defaults to
->set404('path/to/404.php')
// declare controllers
->declareControllers([
  'Me' => new Controllers\MyController(),
])
// TEST
->map('GET', '/test', ['Me', 'test'])
// MATCH or throw 404
->match();

````

---  
## __construct  

### Parameters  

*$basepath* `String`  
Optional. Set the base path if routing from a sub-folder. Defaults to ''.

---  
## set404  

> Chainable  

Sets the 404 page path. If this is not set, 404 responses will be a string: `404: THIS PAGE COULD NOT BE FOUND`.  

### Parameters  

*$path* `String`  
Path to the 404 page template.  

### Return value(s)  
`this`  

---  
## declareControllers  

> Chainable  

Declare controllers using an array.  

### Parameters  

*$controllers* `Array`  
Array containing name and class as key value pairs. Class needs to be 
instantiated when declaring.  

````php
->declareControllers([
  'Me' => new Controllers\MyController(),
]);
````

### Return value(s)  
`this`  

---  
## map  

> Chainable  

Wrapper for AltoRouter::map. Maps request method, route and the target function.  

See [Doc](https://altorouter.com/usage/mapping-routes.html).  

### Parameters  

*$method* `String`  
Pipe-delimited string of the accepted HTTP requests methods.  

Eg: `GET|POST|PATCH|PUT|DELETE`  

*$route* `String`  
This is the route pattern to match against. This can be a plain string, one of the predefined regex filters or a custom regex. Custom regexes must start with @. 

Route | Example Match | Variables  
-- | -- | --  
/contact/ | /contact/ | 
/users/`[i:id]`/ | /users/12/ | `$id: 12` 
/`[a:c]`/`[a:a]?`/`[i:id]?` | /controller/action/21 | `$c: "controller", $a: "action", $id: 21`  

*$target* `String|Array`  
The handler function. Specify a string if referring to a function directly or 
an array if referring to a controller class function (as declared first by 
`declareControllers()`).  

Target | Function  
-- | --  
`myFunction` | `myFunction()`  
`[Me, test]` | `MyController::test()`  

### Return value(s)  
`this`  

---  
## match  

> End of chain  

Matches the router call and invokes the corresponding target function. 
If route is not found, respond with a 404 message or 404 template 
(if set via `set404()`).  
