# SimpleController 

- Designed to be extended by controller classes.  
- Uses SimpleErrorHandler.  
- Example implementation [MyController.php](https://bitbucket.org/johnsonjzhou/simple-php/src/master/template/src/Controllers/MyController.php)  

---  
## Namespace  

`SimplePHP\SimpleServer\SimpleController`  

---  
## Usage  

````php

use SimplePHP\SimpleServer\SimpleController;

class MyController extends SimpleController {

  public function __construct() {
    SimpleController::__construct();
  }

  public function test() {
    try {

      // GET requests 
      // $urlParams = $_GET;

      // POST requests 
      // $this->getRequestParam();
      // $this->getRequestFiles();

      // Security 
      // $this->getClientIp();

      $content = 'hello world';

      $this->setContent($content);

    } catch (\Exception $e) {
      $this->stop($e);
    } finally {
      $this->respondJson();
      // $this->respondTemplate();
      // $this->respondContent();
    }
  }
}

````

---  
## getRequestParam  

Get a specific parameter from within the Request. Handles `multipart/form-data`, `application/json` only.  

### Parameters  

*$param* `String`  
The name of the parameter to get.  

### Return value(s)  
`Mixed` or `null`.  

### Exceptions  
SimplePHP\Exceptions\\`ThrownException`  

---  
## getRequestFiles  

Get an array of files from a Request. Can be from regular `<form>` tag submission or as part of FormData object (flow-interface). For this to work, content-type must be 'multipart/form-data'.  

### Parameters  

*$param* `String`  
The request parameter containing files.  

*$allowedTypes* `String`  
Optional. Regex of allowed MIME types if required. Eg. `/image.*|.pdf/`.  

### Return value(s)  
`Array` of SimplePHP\Resource\\`FileResource`  

### Exceptions  
SimplePHP\Exceptions\\`ThrownException`  

---  
## getClientIp  

Get the Request IP address.  

### Return value(s)  
`String` or `null`.  

---  
## respondTemplate  

Respond with a PHP template.  

### Parameters  

*$template* `String`  
Path to the PHP template page to be used.  

*$parameters* `Array`  
Optional. Parameters to the passed into the page for rendering. This will be 
made available to the template with `extract()`.  

*$statusCode* `Int`  
Optional. The HTTP response code. Default is 200.  

*$headers* `Array`  
Optional. Array of HTTP headers. See [Symfony Docs](https://symfony.com/doc/current/components/http_foundation.html#response).  

---  
## respondJson  

Respond with content as JSON. This should be the default response type for 
a REST end-point.  

### Parameters  

*$this->content* `Mixed`  
Optional. The content to respond with. Any type that can be encoded with 
`json_encode()`. Set by `$this->setContent()` and can be overridden by *$content*.  

*$content* `Mixed`  
Optional. The content to respond with. Any type that can be encoded with 
`json_encode()`. This takes precedence over *$this->content* if both are set.  

*$statusCode* `Int`  
Optional. The HTTP response code. Default is 200.  

*$headers* `Array`  
Optional. Array of HTTP headers. See [Symfony Docs](https://symfony.com/doc/current/components/http_foundation.html#response).  

### Return value(s)  

HTTP response.  

---  
## respondContent  

Respond with raw content based on provided content-type. Use this for 
responses that serves an asset.  

Includes an `ETag` in the response and will respond with status code of 
`304: Not Modified` if `if-none-match` is present in the Request.  

### Parameters  

*$this->content* `Mixed`  
Optional. The content to respond with. Any type that can be encoded with 
`json_encode()`. Set by `$this->setContent()` and can be overridden by *$content*.  

*$content* `Mixed`  
Optional. The content to respond with. Any type that can be encoded with 
`json_encode()`. This takes precedence over *$this->content* if both are set.  

*$contentType* `String`  
Optional. Sets the Content-Type header in the response. Default is text/plain.  

*$statusCode* `Int`  
Optional. The HTTP response code. Default is 200.  

*$headers* `Array`  
Optional. Array of HTTP headers. See [Symfony Docs](https://symfony.com/doc/current/components/http_foundation.html#response).  

### Return value(s)  

HTTP response.  
