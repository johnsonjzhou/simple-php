# Changelog 

## 1.0.0 

Added:   
- added Exception\GeneralFault  
- added Resource\AuthURL  
- added Exception\ThrownException  
- added Exception\OAuthRequired  
- added Exception\FaultPassthrough  
- added SimpleServer\SimpleErrorHandler  
- added SimpleData\SimpleArray  
- added Resource\FileResource  
- added SimpleServer\SimpleMime  
- added SimpleServer\SimpleController  
- added Resource\GuzzleMultipartParam  
- added SimpleServer\SimpleRouter  
- added SimpleServer\SimpleHost  
- added templates for default implementation  
- added SimpleData\MongoDB  
- added Exception\ContentNotFound  
- added Exception\DatabaseUnavailable  
- added Resource\MongoDBDocumentArray  
- added Resource\MongoDBDocument  
- added Resource\MongoDBCollectionSchema  
- added SimpleIntegrations\SimpleSlack  
- added SimpleIntegrations\SimplePinPayments  
- added Resource\PinPaymentsResponse  
- added SimpleController::getRequestInfo  
- added SimpleData\SimpleFile  
- added SimpleIntegrations\SimpleGoogle\GoogleClient  
- added SimpleIntegrations\SimpleGoogle\SimpleGmail  
- added Resource\SimpleEmail  
- added SimpleData\ValidateRegExp  
- added Resource\MailMergeField  
- added Resource\EmailRecipient  
- added Resource\MailSendStatus  
- added SimpleIntegrations\SimpleGoogle\SimpleDrive  
- added SimpleIntegrations\SimpleGoogle\SimpleSheets  
- added Resource\MinfosData  
- added Resource\MinfosDataCustomer  
- added Resource\MinfosDataCustomerIdentifier  
- added Resource\MinfosRequest  
- added Resource\MinfosRequestNode  
- added SimpleIntegrations\SimpleMinfos\MinfosClient  
- added SimpleIntegrations\SimpleMinfos\MinfosCustomer  
- added SimpleIntegrations\SimpleMinfos\MinfosScript  
- added SimpleIntegrations\SimpleMinfos\MinfosExtended  
- added SimpleIntegrations\SimpleMinfos\MinfosApi  
- added SimpleIntegrations\SimpleMinfos\MinfosBookings  

Changes:  
- Reworked GeneralFault and AuthURL exceptions so that AuthURL will always return the url regardless of server environment  
