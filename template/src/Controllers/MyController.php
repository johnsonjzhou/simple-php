<?php
/**
 * This is a template for a controller implemented using SimpleController
 * @author  Johnson Zhou <johnson@simplyuseful.io> 
 */

namespace Controllers;

use SimplePHP\SimpleServer\SimpleController;

class MyController extends SimpleController {

  public function __construct() {
    SimpleController::__construct();
  }

  public function test() {
    try {

      // GET requests 
      // $urlParams = $_GET;

      // POST requests 
      // $this->getRequestParam();
      // $this->getRequestFiles();

      // Security 
      // $this->getClientIp();

      $content = 'hello world';

      $this->setContent($content);

    } catch (\Exception $e) {
      $this->stop($e);
    } finally {
      $this->respondJson();
      // $this->respondTemplate();
      // $this->respondContent();
    }
  }
}

?>