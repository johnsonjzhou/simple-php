<?php 
/**
 * This is a template for index.php implementing SimpleRouter and 
 * SimpleController based controllers 
 * @author  Johnson Zhou <johnson@simplyuseful.io> 
 */

use SimplePHP\SimpleServer\SimpleRouter;

try {

  session_start();

  // LOAD: composer modules
  require_once 'vendor/autoload.php';

  // SET: custom error handler
  // convert all notices, warnings to ErrorException
  set_error_handler(
    'SimplePHP\SimpleServer\SimpleErrorHandler::errorHandler', E_ALL
  );

  // SET: default time zone
  date_default_timezone_set('Australia/Melbourne');

  // GET: project particulars 
  $path = getenv('DOCUMENT_ROOT') ?: '';
  $composer_json = json_decode(file_get_contents($path.'/composer.json'));
  $bugsnag_key = file_get_contents($path.'/.bugsnag');
  $env_mode = file_get_contents($path.'/.env_mode') ?: 'production';

  // SET: environment variables
  putenv("SIMPLE_PHP_ENV_MODE={$env_mode}");
  $bugsnag_key && putenv("SIMPLE_PHP_BUGSNAG={$bugsnag_key}");
  $composer_json && putenv("SIMPLE_PHP_VERSION={$composer_json->version}");

  (new SimpleRouter())
  ->set404('404.php')
  // declare controllers
  ->declareControllers([
    'Me' => new Controllers\MyController(),
  ])
  // TEST
  ->map('GET', '/test', ['Me', 'test'])
  // MATCH or throw 404
  ->match();

} catch (\Exception $e) {
  echo $e;
}

?>