# Simple PHP 

Server side tools written in PHP.  

---
## Version  
1.0.0  (
  [changelog](https://bitbucket.org/johnsonjzhou/simple-php/src/master/doc/changelog.md) | 
  [issues and todo](https://bitbucket.org/johnsonjzhou/simple-php/src/master/doc/issues.md) 
)  

---  
## Installation  

`composer.json`  
````json
"repositories": [
  {
    "type": "vcs",
    "url": "https://johnsonjzhou@bitbucket.org/johnsonjzhou/simple-php.git"
  }
],
````

````bash
composer require johnson/simple-php dev-master
````

---  
## Classes and Namespaces  

Class | Namespace | Doc  
-- | -- | --  
Package | `SimplePHP\`  
SimpleErrorHandler | `SimplePHP\SimpleServer\SimpleErrorHandler` | [Doc](https://bitbucket.org/johnsonjzhou/simple-php/src/master/doc/simple_server/simple_error_handler.md)  
SimpleController | `SimplePHP\SimpleServer\SimpleController` | [Doc](https://bitbucket.org/johnsonjzhou/simple-php/src/master/doc/simple_server/simple_controller.md)  
SimpleRouter | `SimplePHP\SimpleServer\SimpleRouter` | [Doc](https://bitbucket.org/johnsonjzhou/simple-php/src/master/doc/simple_server/simple_router.md)  
SimpleMongoDB | `SimplePHP\SimpleData\SimpleMongoDB` 
SimpleGoogle | `SimplePHP\SimpleIntegrations\SimpleGoogle`  
SimpleSlack | `SimplePHP\SimpleIntegrations\SimpleSlack`  
SimplePinPayments | `SimplePHP\SimpleIntegrations\SimplePinPayments`  
SimpleMinfos | `SimplePHP\SimpleIntegrations\SimpleMinfos`  

---  
## Environment variables  

The following PHP environment variables are referenced. Set via `putenv()`.  

### SIMPLE_PHP_VERSION  

The app version.  

Referenced by:  
- SimpleErrorHandler  

### SIMPLE_PHP_ENV_MODE  

Inspired by `process.env.NODE_ENV` in Node. Carries value of `development` or `production`.  

Referenced by:  
- SimpleServer\SimpleErrorHandler  
- Exception\GeneralFault  
- SimpleIntegrations\SimpleSlack  
- SimpleIntegrations\SimplePinPayments  

### SIMPLE_PHP_BUGSNAG  

If using Bugsnag for error reporting, set this as the api key.  

Referenced by:  
- SimpleErrorHandler  

### SIMPLE_PHP_MONGODB  

The MongoDB database access URI. (optional)  

Referenced by:
- SimpleMongoDB  

---  
## Implementation examples  

Check out [`/template`](https://bitbucket.org/johnsonjzhou/simple-php/src/master/template/).  

---
## License  
MIT.  
Johnson Zhou [johnson@simplyuseful.io](mailto://johnson@simplyuseful.io).