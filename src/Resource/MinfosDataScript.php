<?php
/**
 * Customer object as per Minfos API, 
 * contains the minimum known properties for predictable operation  
 * @author  Johnson Zhou (johnson@simplyuseful.io)
 */
namespace SimplePHP\Resource;

use SimplePHP\Resource\MinfosData;
use SimplePHP\SimpleData\SimpleArray;
use SimplePHP\Resource\MinfosDateTime;

/**
 * @property  string  AuthorityNumber  
 * @property  bool  AuthorityRequired  
 * @property  int  Code  
 * @property  int  CustomerCode  
 * @property  MinfosDateTime  DispensedDate  
 * @property  int  DispensedQuantity  
 * @property  MinfosDateTime  ExpiryDate  
 * @property  float  GovernmentPayment  
 * @property  bool  IsDeferred  
 * @property  bool  IsDeleted  
 * @property  bool  IsGstIncluded  
 * @property  bool  IsOwing 
 * @property  int  Mnpn  
 * @property  int  NumberOfRepeats  
 * @property  string  OriginalPharmacyApprovalNumber  
 * @property  string  OriginalScriptNumber  
 * @property  float  PatientPrice  
 * @property  string  PbsCode  
 * @property  int  PharmacistCode  
 * @property  string  PharmacistInitials  
 * @property  int  PrescriberCode  
 * @property  int  PreviousRepeatScriptCode  
 * @property  int  ProductCode  
 * @property  string  RecallCode  
 * @property  float  SafetyNetAmount  
 * @property  object  SafetyNetNDayRule  
 * @property  MinfosDateTime  ScriptDate  
 * @property  string  ScriptInstructions  
 * @property  string  ScriptNumber  
 * @property  string  ScriptType  Private | Pbs | Rpbs  
 * @property  string  Sig  
 * @property  string  StreamlinedAuthorityCode  
 * @property  int  Tag  
 * @property  int  TimesDispensed  
 * @property  float  WholesaleCost  
 */
class MinfosDataScript extends MinfosData {

  public $AuthorityNumber; 
  public $AuthorityRequired; 
  public $Code; 
  public $CustomerCode; 
  public $DispensedDate; 
  public $DispensedQuantity; 
  public $ExpiryDate; 
  public $GovernmentPayment; 
  public $IsDeferred; 
  public $IsDeleted; 
  public $IsGstIncluded; 
  public $IsOwing; 
  public $Mnpn; 
  public $NumberOfRepeats; 
  public $OriginalPharmacyApprovalNumber; 
  public $OriginalScriptNumber;
  public $PatientPrice; 
  public $PbsCode; 
  public $PharmacistCode; 
  public $PharmacistInitials; 
  public $PrescriberCode; 
  public $PreviousRepeatScriptCode; 
  public $ProductCode; 
  public $RecallCode; 
  public $SafetyNetAmount; 
  public $SafetyNetNDayRule; 
  public $ScriptDate; 
  public $ScriptInstructions; 
  public $ScriptNumber; 
  public $ScriptType; 
  public $Sig; 
  public $StreamlinedAuthorityCode; 
  public $Tag; 
  public $TimesDispensed; 
  public $WholesaleCost; 

  public function __construct($input) {

    $dateFields = [
      'DispensedDate', 'ExpiryDate', 'ScriptDate'
    ];

    (new SimpleArray($dateFields))
    ->forEach(function($field) use (&$input) {
      property_exists($input, $field) && 
        $input->$field = new MinfosDateTime($input->$field);
    });

    MinfosData::__construct($input);
  }
  
}
?>