<?php
/**
 * Displays a list of fields (schema) within a collection of documents 
 * @author  Johnson Zhou <johnson@simplyuseful.io 
 */

namespace SimplePHP\Resource;

use SimplePHP\SimpleData\SimpleArray;
use SimplePHP\Resource\MongoDBDocumentArray;

/**
 * @param  SimplePHP\Resource\MongoDBDocumentArray  $documentArray 
 * 
 * @example  [ { field, count } ]  
 */
class MongoDBCollectionSchema implements \ArrayAccess, \JsonSerializable {

  /** @var  array */
  private $documentArray;

  /** @var  array */
  private $schema = [];

  /** @var  array */
  private $fields = [];

  public function __construct(MongoDBDocumentArray $documentArray) {
    $this->documentArray = $documentArray->get();
    $this->prepare();
  }

  private function getDocumentFields() {
    (new SimpleArray($this->documentArray))
    ->forEach(function($document) {
      // SimplePHP/Resource/MongoDBDocument

      $fields = array_keys($document->getArrayCopy());
      // [ field1, field2, ... ] 

      (new SimpleArray($fields))
      ->forEach(function($field) {
        if (array_key_exists($field, $this->schema)) {
          ++$this->schema[$field];
        } else {
          $this->schema[$field] = 1;
        }
      });
    });

    $this->fields = (new SimpleArray($this->schema))
    ->map(function($value, $field) {
      return (object) [
        "field" => $field, 
        "count" => $value
      ];
    })
    ->get();
  }

  private function prepare() {
    $this->getDocumentFields();
  }

  /**
   * Magic getters 
   */

  public function __serialize() : array {
    return $this->fields;
  }

  public function __toString() {
    $string = '';
    (new SimpleArray($this->fields))
    ->forEach(function($value, $field) use (&$string) { 
      $string .= "\n{$field}({$value})";
    });
    return $string;
  }

  /**
   * Implement JsonSerializable
   */

  public function jsonSerialize() {
    return $this->__serialize();
  }

  /**
   * Implement ArrayAccess
   */

  public function offsetSet($offset, $value) {
    if (is_null($offset)) {
        $this->fields[] = $value;
    } else {
        $this->fields[$offset] = $value;
    }
  }

  public function offsetExists($offset) {
      return isset($this->fields[$offset]);
  }

  public function offsetUnset($offset) {
      unset($this->fields[$offset]);
  }

  public function offsetGet($offset) {
      return isset($this->fields[$offset]) ? $this->fields[$offset] : null;
  }

}

?>