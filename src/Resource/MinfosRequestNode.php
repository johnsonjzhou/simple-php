<?php 
/**
 * Standard reference for a node to be used in MinfosRequest  
 * @author  Johnson Zhou  <johnson@simplyuseful.io>  
 */

namespace SimplePHP\Resource;

class MinfosRequestNode {

  public const NAMESPACE_REQUEST = 'http://tempuri.org/';

  public const NAMESPACE_ARRAY = 
    'http://schemas.microsoft.com/2003/10/Serialization/Arrays';

  public const NAMESPACE_MIDAS_PRODUCT = 
    'http://schemas.datacontract.org/2004/07/NuPro.Midas.Services.EntityServices.Product';

  public const NAMESPACE_MIDAS_CUSTOMER = 
    'http://schemas.datacontract.org/2004/07/NuPro.Midas.Services.EntityServices.Customer';

  public const NAMESPACE_MIDAS_FACILITY = 
    'http://schemas.datacontract.org/2004/07/NuPro.Midas.Services.EntityServices.Facility';

  public const NAMESPACE_MIDAS_SCRIPT = 
    'http://schemas.datacontract.org/2004/07/NuPro.Midas.Services.EntityServices.Script';

  public const NAMESPACE_MIDAS_LEGACY = 
    'http://schemas.datacontract.org/2004/07/LegacyDataAccessService.Models';

  public const NAMESPACE_MIDAS_BOOKINGS = 
    'http://schemas.datacontract.org/2004/07/NuPro.Midas.Services.Bookings.Models';

  public const NAMESPACE_MIDAS_SYSTEM = 
    'http://schemas.datacontract.org/2004/07/System';

  /** @var  string */
  public $name;
  
  /** @var  string */
  public $namespace;

  /** @var  array|int|string */
  public $data;

  /**
   * @param  string  $name  the name of this node as required by the schema  
   * @param  array|int|string  $data  the node data  
   * @param  string  $namespace  the namespace declaration for this node  
   */
  public function __construct(
    string $name = 'Node', 
    $data = [], 
    string $namespace = null
  ) {
    $this->name = $name;
    $this->data = $data;
    $namespace && $this->namespace = $namespace;
  }
}
?>