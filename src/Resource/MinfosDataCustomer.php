<?php
/**
 * Customer object as per Minfos API, 
 * contains the minimum known properties for predictable operation  
 * @author  Johnson Zhou (johnson@simplyuseful.io)
 */
namespace SimplePHP\Resource;

use SimplePHP\Resource\MinfosData;

/**
 * @property  object  Address
 * @property  string  AdmissionDate
 * @property  string  BedNumber
 * @property  int  Code
 * @property  string  ConcessionCardExpiry
 * @property  string  ConcessionCardNumber
 * @property  string  DateOfBirth
 * @property  string  Email
 * @property  object  FacilityKey
 * @property  int  FamilyCode
 * @property  string  FirstName
 * @property  string  HomePhone
 * @property  bool  IsDeleted
 * @property  string  LastName
 * @property  string  MedicareCardExpiry
 * @property  string  MedicareCardNumber
 * @property  string  MobilePhone
 * @property  int  PrescriberCode
 * @property  string  ReleaseDate
 * @property  string  RepatCardExpiry
 * @property  string  RepatCardNumber
 * @property  string  RepatCardType
 * @property  string  SafetyNetAmount
 * @property  string  SafetyNetNumber
 * @property  string  Sex
 * @property  string  Status
 * @property  int  Tag
 * @property  string  Title
 * @property  string  Type
 * @property  string  UnitRecordNumber
 * @property  string  Ward
 * @property  string  WorkPhone
 */
class MinfosDataCustomer extends MinfosData {

  public $Address;
  public $AdmissionDate;
  public $BedNumber;
  public $Code;
  public $ConcessionCardExpiry;
  public $ConcessionCardNumber;
  public $DateOfBirth;
  public $Email;
  public $FacilityKey;
  public $FamilyCode;
  public $FirstName;
  public $HomePhone;
  public $IsDeleted;
  public $LastName;
  public $MedicareCardExpiry;
  public $MedicareCardNumber;
  public $MobilePhone;
  public $PrescriberCode;
  public $ReleaseDate;
  public $RepatCardExpiry;
  public $RepatCardNumber;
  public $RepatCardType;
  public $SafetyNetAmount;
  public $SafetyNetNumber;
  public $Sex;
  public $Status;
  public $Tag;
  public $Title;
  public $Type;
  public $UnitRecordNumber;
  public $Ward;
  public $WorkPhone;

  public function __construct($input) {
    MinfosData::__construct($input);
  }
  
}
?>