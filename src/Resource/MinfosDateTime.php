<?php 
/**
 * An extension of DateTimeImmutable with additional functions to allow 
 * better interfacing with the Minfos API
 * @author  Johnson Zhou  <johnson@simplyuseful.io>  
 */

namespace  SimplePHP\Resource;

/**
 * @method  public  getDate()  
 * @method  public  getDay()  
 * @method  public  getTime()  
 * @method  public  getMinfosTime()  
 * @method  public  getTimestampMS()  
 * @method  public  __toString()  
 * @method  public  __serialize()  
 * @method  public  jsonSerialize()  
 */
class MinfosDateTime extends \DateTimeImmutable implements \JsonSerializable {
  
  public function __construct(
    string  $datetime, 
    \DateTimeZone $timezone = null 
  ) {
    \DateTimeImmutable::__construct($datetime, $timezone);
  }

  /**
   * Formatted date string  
   * @return  string  2021-01-01
   */
  public function getDate(string $format ='Y-m-d') {
    return $this->format($format);
    // 2021-01-01
  }

  /** 
   * Formatted day of week string
   * @return  string  Sunday  
   */
  public function getDay(string $format = 'l') {
    return $this->format($format);
    // Sunday 
  }

  /**
   * Formatted time string  
   * @return  string  01:01 PM
   */
  public function getTime(string $format = 'h:i A') {
    return $this->format($format);
    // 01:01 PM
  }

  /**
   * Formatted date time string consistent with Minfos API  
   * @return  string  2020-12-28T12:05:38
   */
  public function getMinfosTime() {
    $date = $this->format('Y-m-d');
    $time = $this->format('H:i:s');
    return "{$date}T{$time}";
    //2020-12-28T12:05:38
  }

  /**
   * Gets the unix timestamp in milliseconds, to match Javascript  
   * @return  int  
   */
  public function getTimestampMS() {
    return $this->getTimestamp()*1000;
  }

  public function __toString() {
    return $this->getMinfosTime();
  }

  public function __serialize() {
    return $this->getMinfosTime();
  }

  public function jsonSerialize() {
    return $this->__serialize();
  }
}
?>