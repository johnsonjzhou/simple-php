<?php
/**
 * A MailMergeField object  
 * @author  Johnson Zhou (johnson@simplyuseful.io)
 */
namespace SimplePHP\Resource;

/**
 * @property  string  find
 * @property  string  replace
 */
class MailMergeField {
  
  /** @var  string */
  public $find = '';

  /** @var  string */
  public $replace = '';
  
  /**
   * Accept an array or object and maps them to internal properties
   * @param  mixed  $mappings - array or object
   */
  public function __construct($mappings) {

    // if Array, map to Object
    if (gettype($mappings) === 'array')
    $mappings = (object) $mappings;

    if (property_exists($mappings, 'find'))
    $this->find = (string) ($mappings->find);

    if (property_exists($mappings, 'replace'))
    $this->replace = (string) $mappings->replace;

  }
}
?>