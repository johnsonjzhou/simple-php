<?php
/**
 * An extensible Minfos resource to handle responses from the Minfos API
 * in a standardised and predictable way  
 * @author  Johnson Zhou (johnson@simplyuseful.io)
 */
namespace SimplePHP\Resource;

/**
 * @method  jsonSerialize()  
 * @method  __toString()  
 */
class MinfosData implements \JsonSerializable {

  /**
   * @param  object|array  $input  must be iterable by foreach  
   */
  public function __construct($input) {
    foreach ($input as $key => $value) {
      $this->$key = $value;
    }
  }

  private function toArray() : array {
    $array = [];

    foreach ($this as $key => $value) {
      $array[$key] = $value;
    }

    return $array;
  }

  public function jsonSerialize() {
    return $this->toArray();
  }

  public function __toString() : string {
    $string = '';

    foreach ($this as $key => $value) {
      $string .= "{$key}: {$value}";
    }

    return $string;
  }

}
?>