<?php
/**
 * A standard description of whether an email was sent successfully  
 * @author  Johnson Zhou (johnson@simplyuseful.io)
 */
namespace SimplePHP\Resource;

/**
 * @property  bool  send_success
 * @property  string  email
 */
class MailSendStatus {
  
  public $send_success = false;
  public $email = '';
  
  public function __construct() {}
}
?>