<?php
/**
 * A card object conforming to the Cards API
 * @author  Johnson Zhou (johnson@simplyuseful.io)
 * @see  https://pinpayments.com/developers/api-reference/cards
 * 
 * @usage
 * new PinPaymentsCard([... params])
 */
namespace SimplePHP\Resource;

use SimplePHP\Exception\ThrownException;
use SimplePHP\SimpleData\SimpleArray;

/**
 * @throws  SimplePHP\Exception\ThrownException
 */
class PinPaymentsCard {

  public function __construct($input = []) {
    $input = (array) $input;
    
    $required = [
      'number',
      'expiry_month',
      'expiry_year',
      'cvc',
      'name',
      'address_line1',
      'address_city',
      'address_country'
    ];

    $optional = [
      'address_line2',
      'address_postcode',
      'address_state',
    ];

    if (count($input) > 0) {
      // append allowed properties
      (new SimpleArray($input))
      ->forEach(function($value, $key) use ($required, $optional) {
        (in_array($key, $required) || in_array($key, $optional))
          &&
        $this->$key = $value;
      });

      // check for required properties
      (new SimpleArray($required))
      ->forEach(function($property) {
        if (!property_exists($this, $property) || is_null($this->$property))
        throw new ThrownException("{$property} not declared", 400);
      });
    } else {
      throw new ThrownException("Invalid CardObject", 400);
    }
  }
  
}
?>