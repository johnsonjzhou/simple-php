<?php
/**
 * A charge object conforming to the Carges API
 * @author  Johnson Zhou (johnson@simplyuseful.io)
 * @see  https://pinpayments.com/developers/api-reference/charges
 * 
 * @usage
 * new PinPaymentsCharge([... params])
 */
namespace SimplePHP\Resource;

use SimplePHP\Resource\PinPaymentsCard;
use SimplePHP\Exception\ThrownException;
use SimplePHP\SimpleData\SimpleArray;

/**
 * @throws  SimplePHP\Exception\ThrownException  
 */
class PinPaymentsCharge {

  public function __construct($input = []) {
    $input = (array) $input;

    $required = [
      'email',
      'description',
      'amount',
      'ip_address',
    ];

    $optional = [
      'currency',
      'capture',
      'metadata',
    ];

    $one = [
      'card',
      'card_token',
      'customer_token'
    ];

    if (count($input) > 0) {
      // append allowed properties
      (new SimpleArray($input))
      ->forEach(function($value, $key) use ($required, $optional, $one) {
        (
          in_array($key, $required) || 
          in_array($key, $optional) ||
          in_array($key, $one)
        )
          &&
        $this->$key = $value;
      });

      // check for required properties
      (new SimpleArray($required))
      ->forEach(function($property) {
        if (!property_exists($this, $property) || is_null($this->$property))
        throw new ThrownException("{$property} not declared", 400);
      });

      // check for required, but only 'one of' properties
      $singular = (new SimpleArray($one))
      ->filter(function($property) {
        return property_exists($this, $property);
      })
      ->get();
      if (count($singular) !== 1)
      throw new ThrownException(
        "Declare one of card, card_token or customer_token", 400
      );

      // check that card is a PinPaymentsCard
      if (
        property_exists($this, 'card') && 
        !($this->card instanceof PinPaymentsCard)
      )
      throw new ThrownException('card must be a PinPaymentsCard', 400);
      
    } else {
      throw new ThrownException("Invalid PinPaymentsCharge", 400);
    }
  }
}
?>