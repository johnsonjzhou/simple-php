<?php
/**
 * Object containing the authentication URL for OAuth type authentication 
 * @author  Johnson Zhou (johnson@simplyuseful.io)
 * 
 * @class AuthURL 
 * 
 * @public  
 * message - the authentication url 
 * httpCode - 401 unauthorised
 */
namespace SimplePHP\Resource;

use SimplePHP\Exception\GeneralFault;

class AuthURL extends GeneralFault {

  public function __construct(String $url) {

    GeneralFault::__construct(new \Exception($url));
    $this->setHttpCode(401);
    $this->setMessage($url);
  }
}
?>