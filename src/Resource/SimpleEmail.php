<?php
/**
 * Methods for handling email content
 * @author  Johnson Zhou (johnson@simplyuseful.io)
 */

namespace SimplePHP\Resource;

use SimplePHP\SimpleData\SimpleArray;
use SimplePHP\SimpleData\ValidateRegExp as RegExp; 
use SimplePHP\Resource\MailMergeField; 
use SimplePHP\Exception\ThrownException;

/**
 * @method  public  setRecipient()  
 * @method  public  setSender()  
 * @method  public  setReplyTo()  
 * @method  public  setSubject()  
 * @method  public  setContentType()  
 * @method  public  setContent()  
 * @method  public  mapContentFields()  
 * @method  public  create()  
 * @method  public  getBase64()  
 * @method  public  getPlain()  
 * 
 * @example  usage  
 * (new SimpleEmail())  
 * ->setRecipient(email, name)  
 * ->setSender(email, name)  
 * ->setSubject(subject)  
 * ->setContentType(contentType) optional  
 * ->setContent(content)  
 * ->mapContentFields(array of fields, data)  
 * ->create()  
 * ->getBase64()  
 */
class SimpleEmail {

  /** @var  string */
  private $recipientName = '';

  /** @var  string */
  private $recipientEmail = '';

  /** @var  string */
  private $senderName = '';

  /** @var  string */
  private $senderEmail = '';

  /** @var  string */
  private $replyTo = '';

  /** @var  string */
  private $subject = '';

  /** @var  string */
  private $contentType = 'text/html';

  /** @var  string */
  private $content = '';

  /** @var  string */
  private $email;

  public function __construct() {}

  /**
   * Setter,
   * Set the To field using an email address
   * @chainable
   * @param  string  $email - recipient email address
   * @param  string  $name - recipient name
   * 
   * @return  this
   */
  public function setRecipient(
    String $email,
    String $name = ''
  ) {
    // handle $email
    if (is_null($email))
      throw new ThrownException('Email is not defined');

    // validate $email
    if (!RegExp::validate($email, 'email'))
      throw new ThrownException('Not valid email format');
    
    // set recipientEmail
    $this->recipientEmail = $email;

    // set recipientName
    $this->recipientName = $name;

    return $this;
  }

  /**
   * Setter,
   * Set the From field using an email address
   * @chainable
   * @param  string  $email - sender email address
   * @param  string  $name - sender name
   * 
   * @return  this
   */
  public function setSender(
    String $email,
    String $name = ''
  ) {
    // handle $email
    if (is_null($email))
      throw new ThrownException('Sender is not defined');

    // validate $email
    if (!RegExp::validate($email, 'email'))
      throw new ThrownException('Sender not valid email format');
    
    // set senderEmail
    $this->senderEmail = $email;

    // set senderName
    $this->senderName = $name;

    return $this;
  }

  /**
   * Setter,
   * Set the Reply To field using an email address
   * @chainable
   * @param  string  $email - reply-to email address
   * 
   * @return  this
   */
  public function setReplyTo(
    String $email
  ) {
    // handle $email
    if (is_null($email))
      throw new ThrownException('Reply-To is not defined');

    // validate $email
    if (!RegExp::validate($email, 'email'))
      throw new ThrownException('Reply-To not valid email format');
    
    // set replyTo
    $this->replyTo = $email;

    return $this;
  }

  /**
   * Setter,
   * Set the Subject field using as plain text
   * @chainable
   * @param  string  $subject - the subject text
   * 
   * @return  this
   */
  public function setSubject(
    String $subject
  ) {
    // handle $subject
    if (is_null($subject))
      throw new ThrownException('Subject is not defined');

    // validate $subject
    if (!RegExp::validate($subject, 'text'))
      throw new ThrownException('Subject is not in valid text format');
    
    // set subject
    $this->subject = $subject;

    return $this;
  }

  /**
   * Setter,
   * Set the content type of the email body
   * @chainable
   * @param  string  $contentType - default text/html
   * 
   * @return  this
   * 
   * @example  $contentType
   * text/html | text/plain
   */
  public function setContentType(
    String $contentType = 'text/html'
  ) {
    // set content
    $this->contentType = $contentType;

    return $this;
  }

  /**
   * Setter,
   * Set the content or email body
   * @chainable
   * @param  string  $content - email body
   * 
   * @return  this
   */
  public function setContent(
    String $content
  ) {
    // handle $content
    if (is_null($content))
    throw new ThrownException('Content is not defined');
    
    // set content
    $this->content = $content;

    return $this;
  }

  /**
   * Setter,
   * Takes an array of MailMergeFieldObjects 
   * and replaces them within the content
   * @chainable
   * @param  array  of MailMergeField(s)
   * @param  string  $this->content
   * 
   * @return  this
   */
  public function mapContentFields(Array $mergeFields = []) {
    // skip if blank $this->content
    if ($this->content === '') return;

    (new SimpleArray($mergeFields))
    ->forEach(
      function($mergeField) {
        if (!($mergeField instanceof MailMergeField)) return;

        $this->content = str_replace(
          $mergeField->find,
          $mergeField->replace,
          $this->content
        );
      }
    );

    return $this;
  }

  /**
   * Encodes the content to Base64URL standard, necessary for email bodies  
   * @param  string  $string - the created email string
   * 
   * @return  string  
   * 
   * Base64URL encode the content string
   * @see http://en.wikipedia.org/wiki/Base64#Implementations_and_history
   */
  private function encodeBase64URL(String $string) {
    // handle $string
    if (is_null($string))
    throw new ThrownException('No string provided to encode');

    return
      strtr(base64_encode($string), array('+' => '-', '/' => '_'));
  }

  /**
   * Setter,
   * Creates and sets the email as a formatted string,
   * Encodes content portion as quoted-printable
   * @param  string  $this->recipientName
   * @param  string  $this->recipientEmail
   * @param  string  $this->senderEmail
   * @param  string  $this->subject
   * @param  string  $this->contentType
   * @param  string  $this->content
   * 
   * @return  this
   * 
   * Base64url encode the content string
   * @see http://en.wikipedia.org/wiki/Base64#Implementations_and_history
   */
  public function create() {
    $email = '';

    // handle $this->recipientName
    if (is_null($this->recipientName))
      throw new ThrownException('Recipient name has not been set');

    // handle $this->recipientEmail
    if (is_null($this->recipientEmail))
      throw new ThrownException('Recipient email has not been set');

    // handle $this->senderEmail
    if (is_null($this->senderEmail))
      throw new ThrownException('Sender email has not been set');

    // handle $this->subject
    if (is_null($this->subject))
      throw new ThrownException('Subject has not been set');
    
    // handle $this->content
    if (is_null($this->content))
      throw new ThrownException('Content has not been set');

    // To
    $email .= "To: {$this->recipientName} <{$this->recipientEmail}>\r\n";

    // From
    $email .= "From: {$this->senderName} <{$this->senderEmail}>\r\n";

    // Reply-To
    if ($this->replyTo !== '')
    $email .= "Reply-To: <{$this->replyTo}>\r\n";

    // Subject
    $email .= "Subject: {$this->subject}\r\n";

    // MIME & Content type
    $email .= "MIME-Version: 1.0\r\n";
    $email .= "Content-Type: {$this->contentType}; charset=utf-8\r\n";
    $email .= "Content-Transfer-Encoding: quoted-printable\r\n\r\n";

    // Content
    $email .= quoted_printable_encode($this->content);

    $this->email = $email;

    return $this;
  }

  /**
   * Getter,
   * returns the email, encoded as Base64URL
   * @param  string  $this->email
   * 
   * @return  string
   */
  public function getBase64() {
    // handle $this-email
    if (is_null($this->email))
    throw new ThrownException('Email has not been created');

    return $this->encodeBase64URL($this->email);
  }

  /**
   * Getter,
   * returns the email, encoded as Base64URL
   * @param  string  $this->email
   * 
   * @return  string
   */
  public function getPlain() {
    // handle $this-email
    if (is_null($this->email))
    throw new ThrownException('Email has not been created');

    return $this->email;
  }
}

?>