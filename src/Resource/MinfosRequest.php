<?php  
/**
 * A standard request object consisting of SoapVar objects 
 * that can be used with MinfosClient (SoapClient) 
 * 
 * @author  Johnson Zhou  <johnson@simplyuseful.io>
 */

namespace SimplePHP\Resource;

use SimplePHP\Resource\MinfosRequestNode as Node;
use SimplePHP\SimpleData\SimpleArray;

use \SoapVar;

class MinfosRequest {

  /**
   * @param  string  $parameterName  
   *  name of the request parameter per Minfos API docs  
   * 
   * @param  array  $data  
   *  the request data, comprising of MinfosRequestNode objects  
   * 
   * @param  string  $nameSpace  
   *   namespace declaration as per the endpoint schema  
   * 
   * @see  SimplePHP\Resource\MinfosRequestNode  
   *   for common namespaces as constants  
   */
  public function __construct(
    string $parameterName = '', 
    array $data = [], 
    string $nameSpace = Node::NAMESPACE_REQUEST
  ) {
    $nodeData = $this->transformNode($data);
    $this->$parameterName = new SoapVar(
      $nodeData, SOAP_ENC_OBJECT, null, null, $parameterName, $nameSpace
    );
  }

  /** 
   * Transforms MinfosRequestNode into SoapVar. 
   * Will be called recursively if the node data is an array.  
   * 
   * @return  array  
   */
  private function transformNode(array $data) {
    return (new SimpleArray($data))
    ->map(function($node) {
      
      if (!$node instanceof Node) return null;

      $name = $node->name;
      $namespace = $node->namespace;
      $data = $node->data;

      switch (gettype($data)) {
        case 'array':
          $nestedData = $this->transformNode($data);
          return new SoapVar(
            $nestedData, SOAP_ENC_OBJECT, null, null, $name, $namespace
          );

        case 'string': 
          return new \SoapVar(
            $data, XSD_STRING, null, null, $name, $namespace
          );
  
        case 'integer': 
          return new \SoapVar(
            $data, XSD_INT, null, null, $name, $namespace
          );
      }
    })
    ->filter(function($node) { return !is_null($node); })
    ->get();
  }
}
?>