<?php
/**
 * Identifier object as per Minfos API, 
 * contains the minimum known properties for predictable operation  
 * @author  Johnson Zhou (johnson@simplyuseful.io)
 */
namespace SimplePHP\Resource;

use SimplePHP\Resource\MinfosData;

/**
 * @property  int  Code
 * @property  int  Tag
 */
class MinfosDataIdentifier extends MinfosData {

  public $Code;
  public $Tag;

  public function __construct($input) {
    MinfosData::__construct($input);
  }
  
}
?>