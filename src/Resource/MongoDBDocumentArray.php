<?php
/**
 * Standard response for an array of MongoDB documents 
 * @author  Johnson Zhou <johnson@simplyuseful.io
 */

namespace SimplePHP\Resource;

use \MongoDB\Driver\Cursor;
use SimplePHP\SimpleData\SimpleArray;
use SimplePHP\Resource\MongoDBDocument;

/**
 * Converts Cursor object to array  
 * Converts ObjectId to a string  
 * 
 * @method  get()  
 * @method  __serialise()  
 * @method  jsonSerialize()  
 * @method  __toString()  
 * @method  offsetSet()  
 * @method  offsetExists()  
 * @method  offsetUnset()  
 * @method  offsetGet()  
 */
class MongoDBDocumentArray implements \ArrayAccess, \JsonSerializable {

  /** @var \MongoDB\Driver\Cursor  */
  private $cursor; 

  /** @var array */
  private $array = [];

  /**
   * @param  \MongoDB\Driver\Cursor  $cursor
   */
  public function __construct(Cursor $cursor) {
    $this->cursor = $cursor;
    $this->prepare();
  }

  private function toArray() {
    $this->array = iterator_to_array($this->cursor);
  }

  private function convertDocument() {
    $this->array = (new SimpleArray($this->array))
    ->map(function($document) {
      // transform SimplePHP\Resource\MongoDBDocument
      return new MongoDBDocument($document);
    })
    ->get();
  }

  private function prepare() {
    $this->toArray();
    $this->convertDocument();
  }

  /**
   * Getter, returns the document array 
   * @return  array  
   */
  public function get() : array {
    return $this->array;
  }

  /**
   * Magic getters
   */

  public function __serialize() : array {
    return $this->array;
  }

  public function __toString() {
    $count = count($this->array);
    return get_class($this)."({$count})";
  }

  /**
   * JsonSerializable interface  
   */

  public function jsonSerialize() {
    return $this->__serialize();
  }

  /**
   * ArrayAccess interface  
   */

  public function offsetSet($offset, $value) {
    if (is_null($offset)) {
        $this->array[] = $value;
    } else {
        $this->array[$offset] = $value;
    }
  }

  public function offsetExists($offset) {
      return isset($this->array[$offset]);
  }

  public function offsetUnset($offset) {
      unset($this->array[$offset]);
  }

  public function offsetGet($offset) {
      return isset($this->array[$offset]) ? $this->array[$offset] : null;
  }
}