<?php
/**
 * An object containing:
 * an fopen() resource, 
 * and filename
 * @author  Johnson Zhou (johnson@simplyuseful.io)
 * 
 * @method  setters
 * setFilename
 * setContents
 * 
 * @properties
 * filename
 * contents
 */

namespace SimplePHP\Resource;

/**
 * @public  filename
 * @public  contents - fopen() resource 
 * 
 * @method  setFilename 
 * @method  setContents 
 */
class FileResource {

  public $filename;
  public $contents;

  public function __construct() {}

  /**
   * @chainable
   * @return  this
   */
  public function setFilename(String $filename) {
    $this->filename = $filename;
    return $this;
  }

  /**
   * @chainable
   * @return  this
   */
  public function setContents($contents) {
    $this->contents = $contents;
    return $this;
  }
}
?>