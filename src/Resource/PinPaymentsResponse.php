<?php
/**
 * A standardised way to respond to the client from the Pin Payments API.
 * @author  Johnson Zhou <johnson@simplyuseful.io>  
 * 
 * @see  https://pinpayments.com/developers/api-reference
 * 
 * @example success response from API
 * {
 *  "response" : {
 *    "key": "value"
 *   }
 * }
 * 
 * @example error response from API
 * {
 *  "error": "an_error_code",
 *  "error_description": "A description of the error."
 * }
 */
namespace SimplePHP\Resource;

/**
 * A standard response object for the frontend client from the Pin Payments API 
 * 
 * @method  __serialise()  
 * @method  jsonSerialize()  
 * @method  __toString()  
 * 
 * @example  standard response  
 * [
 *  "status" => int, 
 *  "ok" => bool, 
 *  "test_mode" => bool, 
 *  ... flattened from original API response 
 * ]
 */
class PinPaymentsResponse implements \JsonSerializable {

  /** @var  int */
  public $status;

  /** @var  bool */
  public $ok = true;

  /** @var  bool */
  public $test_mode = true;

  /** @var  object */
  private $content;

  /**
   * @param  object|array  $content - usually from json_decode  
   * @param  int  $status - default 403
   * @param  bool  $testMode - default true  
   */
  public function __construct($content = [], $status = 403, $testMode = true) {
    $this->status = $status;
    $this->test_mode = $testMode;
    $this->content = (object) $content;
    $this->prepare();
  }

  private function prepare() {

    // success response from API
    if (property_exists($this->content, 'response')) {
      $this->ok = true;
      foreach ($this->content->response as $key => $value) {
        $this->$key = $value;
      }
    }

    // error response from API  
    if (property_exists($this->content, 'error')) {
      $this->ok = false;
      foreach ($this->content as $key => $value) {
        $this->$key = $value;
      }
    }
  }

  public function __serialize() {
    return $this;
  }

  public function jsonSerialize() {
    return $this->__serialize();
  }

  public function __toString() {
    return get_class($this);
  }
}
?>