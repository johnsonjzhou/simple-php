<?php
/**
 * Standardised fields associated with an email recipient  
 * @author  Johnson Zhou (johnson@simplyuseful.io)
 */
namespace SimplePHP\Resource;

/**
 * @property  string  email
 * @property  string  firstname
 * @property  string  surname
 */
class EmailRecipient {
  
  public $email = '';
  public $firstname = '';
  public $surname = '';

  /**
   * Accept an array or object and maps them to internal properties
   * @param  mixed  $mappings - array or object 
   * 
   * @example  [ email, firstname, surname ]
   */
  public function __construct($mappings) {

    // if Array, map to Object
    if (gettype($mappings) === 'array')
    $mappings = (object) $mappings;

    if (property_exists($mappings, 'email'))
    $this->email = (string) ($mappings->email);

    if (property_exists($mappings, 'firstname'))
    $this->firstname = (string) $mappings->firstname;

    if (property_exists($mappings, 'surname'))
    $this->surname = (string) $mappings->surname;

  }
}
?>