<?php
/**
 * Standard response for a MongoDB Document that converts data 
 * within a BSONDocument to feed back to the client 
 * @author  Johnson Zhou <johnson@simplyuseful.io>
 */

namespace SimplePHP\Resource;

use \MongoDB\BSON\ObjectId;

/**
 * Converts ObjectId to a string  
 * 
 * @method  __serialise()  
 * @method  jsonSerialize()  
 * @method  __toString()  
 */
class MongoDBDocument extends \ArrayObject implements \JsonSerializable {

  public function __construct($input = [], $flags = \ArrayObject::ARRAY_AS_PROPS, $iterator_class = 'ArrayIterator') {
    \ArrayObject::__construct($input, $flags, $iterator_class);
  }

  /**
   * Transform MongoDB\BSON\ObjectId to string 
   */
  private function stringifyObjectId() {
    foreach ($this as $key => $value) {
      if ($value instanceof ObjectId) {
        $this[$key] = (string) $value;
      }
    }
  }

  private function prepare() {
    $this->stringifyObjectId();
  }

  public function __serialize() {
    $this->prepare();
    return (object) $this->getArrayCopy();
  }

  public function jsonSerialize() {
    return $this->__serialize();
  }

  public function __toString() {
    return get_class($this);
  }
} 

?>