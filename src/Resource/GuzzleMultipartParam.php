<?php
/**
 * Managing multipart/form-data form params key value pairs,
 * for GuzzleHttp/RequestOptions
 * @see  http://docs.guzzlephp.org/en/stable/request-options.html#multipart
 * 
 * @method  getter
 * toArray
 */
namespace SimplePHP\Resource;

/**
 * @param  string  $name
 * @param  StreamInterface|resource|string  $contents
 * @param  array  $headers - optional
 * @param  string  $filename
 * 
 * @see  http://docs.guzzlephp.org/en/stable/request-options.html#multipart
 */
class GuzzleMultipartParam {

  public $name;
  public $contents;

  public function __construct(
    String $name,
    $contents,
    Array $headers = [],
    String $filename = null
  ) {
    $this->name = $name;
    $this->contents = $contents;
    if (count($headers) > 0) $this->headers = $headers;
    if (!is_null($filename)) $this->filename = $filename;
  }

  /**
   * @return  array
   */
  public function toArray() {
    return (array) $this;
  }
}
?>