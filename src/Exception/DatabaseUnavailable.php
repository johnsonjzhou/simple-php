<?php
/**
 * Exception for when Database is not available
 * @author  Johnson Zhou <johnson@simplyuseful.io>
 */
namespace SimplePHP\Exception;
use SimplePHP\Exception\ThrownException;

class DatabaseUnavailable extends ThrownException {

  /**
   * @param  string  $message - default "Database is not available" 
   * @param  int  $code - default "503"
   */
  public function __construct(
    string $message = 'Database is not available',
    int $code = 503
  ) {
    ThrownException::__construct($message, $code);
  }

}
?>