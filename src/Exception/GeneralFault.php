<?php
/**
 * An object to describe a GeneralFault in SimpleErrorHandler  
 * @author  Johnson Zhou (johnson@simplyuseful.io)
 */

namespace SimplePHP\Exception;

/**
 * @param  \Exception  $e
 * 
 * @property  string  message
 * @property  string  file
 * @property  string  line
 * @property  string  class  
 * @property  int  httpCode  
 * 
 * @method  setHttpCode()  
 * @method  setMessage()  
 * @method  jsonSerialize()  
 * @method  __toString()  
 */
class GeneralFault implements \JsonSerializable {

  public $message = 'General server error';
  public $file = null;
  public $line = null;
  public $class = null;
  public $httpCode = 500;

  public function __construct(\Exception $e) {

    if (getenv('SIMPLE_PHP_ENV_MODE') === 'development') {
      $this->message = $e->getMessage();
      $this->file = $e->getFile();
      $this->line = $e->getLine();
      $this->class = get_class($e);
    };

  }

  /**
   * Sets the httpCode
   * @param  int  httpCode
   * @return  null
   */
  public function setHttpCode(int $httpCode = 500) {
    $this->httpCode = $httpCode;
    return null;
  }

  /**
   * Sets the error message
   */
  public function setMessage(string $message) {
    $this->message = $message;
  }

  public function jsonSerialize() : array {

    $array = [
      'message' => $this->message, 
    ];

    if (getenv('SIMPLE_PHP_ENV_MODE') === 'development') {
      $this->class && $array['class'] = $this->class;
      $this->file && $array['file'] = $this->file;
      $this->line && $array['line'] = $this->line;
    };

    return $array;
    
  }

  public function __toString() {

    $string = $this->message;

    if (getenv('SIMPLE_PHP_ENV_MODE') === 'development') {
      $this->class && $string .= "\nClass: {$this->class}";
      $this->file && $string .= "\nFile: {$this->file}";
      $this->line && $string .= "\nLine: {$this->line}";
    }

    return $string;
  }
}
?>