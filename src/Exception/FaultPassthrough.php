<?php
/**
 * Pass through an existing GeneralFault 
 * @author  Johnson Zhou (johnson@simplyuseful.io)
 * 
 * @class FaultPassthrough implements \Exception
 */
namespace SimplePHP\Exception;

use SimplePHP\Exception\GeneralFault;

/**
 * @param  GeneralFault  $fault
 */
class FaultPassthrough extends \Exception {

  public function __construct(GeneralFault $fault) {

    \Exception::__construct($fault->message);

  }

}
?>