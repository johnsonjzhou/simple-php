<?php
/**
 * Exception for when the requested content was not found in the Database
 * @author  Johnson Zhou <johnson@simplyuseful.io>
 */
namespace SimplePHP\Exception;
use SimplePHP\Exception\ThrownException;

class ContentNotFound extends ThrownException {

  /**
   * @param  string  $message - default "Content Not Found" 
   * @param  int  $code - default "404"
   */
  public function __construct(
    string $message = 'Content not found',
    int $code = 404
  ) {
    parent::__construct($message, $code);
  }

}
?>