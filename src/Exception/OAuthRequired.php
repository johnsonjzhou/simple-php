<?php
/**
 * Exception for handling OAuth type URL callbacks
 * @author  Johnson Zhou (johnson@simplyuseful.io)
 * 
 * @class AuthRequiredFault  
 * 
 * @method  public 
 * getURL
 */
namespace SimplePHP\Exception;

use SimplePHP\Exception\ThrownException;

/**
 * @param  string  $url - the authentication url
 * @param  int  $code - default 401 unauthorised 
 */
class OAuthRequired extends ThrownException {

  public function __construct(
    String $url = 'OAuth authentication is required', 
    Int $code = 401
  ) {
    // the url will be set as the 'message' property of the ThrownException
    ThrownException::__construct($url, $code);
  }

  /**
   * Retrieves the URL, which is stored as the 'message' property 
   */
  public function getURL() {
    return $this->getMessage();
  }
}
?>