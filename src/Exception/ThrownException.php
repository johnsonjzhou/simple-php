<?php
/**
 * ThrownException to distinguish from regular caught \Exception
 * @author  Johnson Zhou (johnson@simplyuseful.io)
 */
namespace SimplePHP\Exception;

/**
 * @param  string  $message
 * @param  int  $code - using a http status code is more helpful
 */
class ThrownException extends \Exception {

  public function __construct(
    string $message = 'Exception thrown',
    int $code = 500
  ) {
    \Exception::__construct($message, $code);
  }

}
?>