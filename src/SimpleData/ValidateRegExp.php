<?php
/**
 * Provides RegExp based pattern validation
 * @author  Johnson Zhou (johnson@simplyuseful.io)
 */
namespace SimplePHP\SimpleData;

/**
 * @property  array  self::PATTERN    
 * @method  public static  self::validate()  
 */
class ValidateRegExp {

  /** @var  array */
  public const PATTERN = [
    'email' => '/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/',
    'password' => '/^\S*$/i',
    'tel' => '/^\({0,1}((0|\+61|\+61\s)(2|4|3|7|8)){1}\){0,1}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{1}(\ |-){0,1}[0-9]{3}$/',
    'number' => '/^[0-9]{0,}$/',
    'eol' => '/\r\n|\r|\n/',
    'text' => '/^[a-z0-9\s"' . "'’,.-]+$/i",
    'textarea' => '/^[a-z0-9\s"' . "'’,.-:*#$@%?()]+$/i",
  ];

  public function __construct() {}

  /**
   * Validates the $subject based on specified pattern $type
   * @param  string  $subject
   * @param  string  $type
   * 
   * @return  bool
   * 
   * @example  $type  
   * email | password | tel | number | eol | text | textarea  
   * Will return false if invalid type is supplied  
   */
  public static function validate(String $subject, String $type) {
    try {
      // assume failure
      $valid = false;

      // get pattern
      $pattern = self::PATTERN[$type];
      if (!$pattern) return;

      // match the pattern
      $match = preg_match($pattern, $subject);
      if ($match === 1) $valid = true;
      
    } catch (\Exception $e) {
    } finally {
      return $valid;
    }
  }
}
?>