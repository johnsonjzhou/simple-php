<?php
/**
 * Simple and chainable PHP file management utilities
 * @author  Johnson Zhou (johnson@simplyuseful.io)
 */

namespace SimplePHP\SimpleData;

use SimplePHP\Exception\ThrownException;

/**
 * @method  get()  
 * @method  exists()  
 * @method  explodeEOL  
 * @method  getPath()  
 * @method  close()  
 * @method  delete()
 * @method  jsonDecode()  
 * 
 * @method  read()  chainable  
 * @method  write()  chainable  
 * @method  append()  chainable  
 * @method  create()  chainable  
 * @method  jsonEncode()  chainable  
 * 
 */
class SimpleFile {

  /** @var  string */
  private $path = '';

  /** @var  resource -- file pointer resource created by fopen */
  private $file;

  /** @var  string */
  private $contents = '';

  /**
   * Upon loading:
   * check DOCUMENT_ROOT
   * check file $path
   * 
   * ! Not compatible with windows paths.  
   * 
   * @param  string  $path  the file system path to the file, 
   *   can be absolute system path, or 
   *   prefix with "DOCUMENT_ROOT/" for relative path 
   * @param  string  $_ENV['DOCUMENT_ROOT']  
   * 
   * @throws  SimplePHP\Exception\ThrownException
   */
  public function __construct(String $path = null) {
    // handle DOCUMENT_ROOT
    if (!getenv('DOCUMENT_ROOT'))
      throw new ThrownException('Server incorrectly configured');

    // handle $path
    if (is_null($path))
      throw new ThrownException('File path not specified');

    // assign $this->path
    // check for DOCUMENT_ROOT at the start of $path  
    switch (strpos($path, 'DOCUMENT_ROOT') === 0) {
      case true:
        // path provided is relative to DOCUMENT_ROOT
        // strip DOCUMENT_ROOT, then attach full path
        $this->path = getenv('DOCUMENT_ROOT').trim($path, 'DOCUMENT_ROOT');
        break;

      case false:
        // path provided is the full server system path
        $this->path = $path;
        break;
    }
  }

  /**
   * Functions to check if a file
   * exist, readable, writable or filesize
   * @return  bool
   */
  public function exists() {
    return file_exists($this->path);
  }

  private function readable() {
    return is_readable($this->path);
  }

  /**
   * @param  bool  $checkFolder
   */
  private function writable(Bool $checkFolder = false) {
    if ($checkFolder) {
      $check = substr(
        $this->path,
        0,
        strrpos($this->path,'/')
      );
    } else {
      $check = $this->path;
    }
    return is_writable($check);
  }

  private function filesize() {
    return filesize($this->path);
  }

  /**
   * Getter, concludes the chain
   * @return  string  $this->contents
   */
  public function get() {
    ($this->file) && fclose($this->file);
    return $this->contents;
  }

  /**
   * Getter, returns the full path,
   * concludes the chain
   * @return  string  $this->path
   */
  public function getPath() {
    ($this->file) && fclose($this->file);
    return $this->path;
  }

  /**
   * Getter,
   * splits the contents by the EOL markers and returns in an array,
   * concludes the chain
   * @return  array  $this->contents
   */
  public function explodeEOL() {
    ($this->file) && fclose($this->file);
    return preg_split('/\r\n|\r|\n/', $this->contents);
  }

  /**
   * Getter,
   * retrieves the file contents and decodes as a json object,
   * concludes the chain
   * 
   * @param  bool  $assoc  converted into associative arrays  
   * @param  int  $depth  recursion depth  
   * @param  int  $options  Bitmask of JSON decode options  
   * 
   * @see https://www.php.net/manual/en/function.json-decode.php  
   * 
   * @return  string  $this->contents  
   */
  public function jsonDecode(
    Bool $assoc = false,
    Int $depth = 512,
    Int $options = 0
    ) {
      ($this->file) && fclose($this->file);
      return json_decode($this->contents, $assoc, $depth, $options);
  }

  /**
   * Setter,
   * creates a json object from a defined value
   * @param  mixed  $value
   * 
   * @throws  SimplePHP\Exception\ThrownException
   * 
   * @return  this  
   */
  public function jsonEncode($value = null) {
    // handle empty value
    if (is_null($value))
      throw new ThrownException('No value defined');
    
    $this->contents = json_encode($value);

    return $this;
  }

  /**
   * Closes the file pointer stream and the chain without returning anything
   * @return  bool  true if successful
   */
  public function close() {
    return fclose($this->file);
  }

  /**
   * Reads a file and returns it's contents
   * @chainable
   * 
   * @param  int  $length - Up to $length number of bytes read  
   * 
   * @return  this  
   * 
   * @throws  SimplePHP\Exception\ThrownException  
   */
  public function read(Int $length = null) {
    // reset file stream operation
    ($this->file) && fclose($this->file);

    // check file exists
    if (!$this->exists())
      throw new ThrownException('File does not exist');

    // check file is readable
    if (!$this->readable())
      throw new ThrownException('File not readable');

    // open file in read only mode
    if (!$this->file = fopen($this->path, 'r'))
      throw new ThrownException('Error opening file');

    // lock the file
    if (!flock($this->file, LOCK_SH))
      throw new ThrownException('Could not lock file');

    // handle length
    switch ($length) {
      case null: 
        // not set in params
        $length = $this->filesize();
        break;
      
      case 0:
        // blank file 
        $length = 1;
        break;
    }

    $this->contents = fread($this->file, $length);

    // unlock the file
    flock($this->file,  LOCK_UN);

    return $this;
  }

  /**
   * Overwrites an existing file or create a new file then write
   * @chainable
   * 
   * @param  string  $chunk - string to write to the file
   * @param  bool  $overwrite - allows an existing file to be overwritten
   * @note: default is to allow overwriting of existing file
   * ! might need to review the mutation of $this->contents
   * 
   * @return  this  
   * 
   * @throws  SimplePHP\Exception\ThrownException  
   */
  public function write(String $chunk = '', Bool $overwrite = true) {
    // reset file stream operation
    ($this->file) && fclose($this->file);

    // throw an error if we could not write to the specified file's folder
    if (!$this->writable(true))
      throw new ThrownException('Could not write to destination folder');

    // throw error if file exists and not specified to overwrite
    if ($this->exists() && !$overwrite)
      throw new ThrownException('Not specified to overwrite to existing file');

    // throw error if exists, overwriting but not writable
    if ($this->exists() && $overwrite && !$this->writable())
      throw new ThrownException('Existing file not writable');

    // open or create file to write and read
    if (!$this->file = fopen($this->path, 'w+'))
      throw new ThrownException('File open error');

    // lock file
    if (!flock($this->file, LOCK_EX))
      throw new ThrownException('Could not lock file');

    // handle chunk
    if ($chunk === '') {
      if (!is_null($this->contents)) {
        // chunk is not provided but we have contents
        $chunk = $this->contents;
      } else {
        throw new ThrownException('No data chunk to write');
      }
    }

    // write chunk to file
    if (!$this->contents = fwrite($this->file, utf8_encode($chunk)))
      throw new ThrownException('Error writing to file');

    // unlock the file
    flock($this->file,  LOCK_UN);

    return $this;
  }

  /**
   * Adds to the back of an existing file
   * @chainable
   * 
   * @param  string  $chunk - string to write to the file
   * @param  bool  $create - create the file if it does not exist
   * 
   * @return  this  
   * 
   * @throws  SimplePHP\Exception\ThrownException  
   */
  public function append(String $chunk = '', Bool $create = false) {
    // reset file stream operation
    ($this->file) && fclose($this->file);
      
    // check file exists and create flag is false
    if (!$this->exists() && !$create)
      throw new ThrownException('File does not exist');

    // check file is writable
    if (!$this->writable())
      throw new ThrownException('File is not writable');

    // open or create file to append and read
    if (!$this->file = fopen($this->path, 'a+'))
      throw new ThrownException('File open error');

    // lock file
    if (!flock($this->file, LOCK_EX))
      throw new ThrownException('Could not lock file');

    // write chunk to file
    if (!$this->contents = fwrite($this->file, utf8_encode($chunk)))
      throw new ThrownException('Error writing to file');

    // unlock the file
    flock($this->file,  LOCK_UN);

    return $this;
  }

  /**
   * Creates a file
   * @chainable
   * 
   * @param  bool  $overwrite - overwrite any existing file
   * 
   * @return  this  
   * 
   * @throws  SimplePHP\Exception\ThrownException  
   */
  public function create(Bool $overwrite = false) {
    // reset file stream operation
    ($this->file) && fclose($this->file);

    // check if file exists
    if ($this->exists() && !$overwrite)
      throw new ThrownException('File exists');

    // create file
    if (!fopen($this->path, 'w+'))
      throw new ThrownException('Error creating file');

    return $this;
  }

  /**
   * Deletes a file,
   * end of chain
   * @return  bool  
   */
  public function delete() {
    if (
      $this->exists() &&
      $this->writable()
    ) {
      unlink($this->path);
      return true;
    } else {
      return false;
    }
  }
}

?>
