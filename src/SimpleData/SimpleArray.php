<?php
/**
 * Wrapper for PHP array functions so they can be chained and behaves
 * more like Javascript array functions
 * @author  Johnson Zhou  (johnson@simplyuseful.io)
 * 
 * @class SimpleArray
 * 
 * @uses  SimplePHP\SimpleServer\SimpleErrorHandler
 * 
 * @method public
 * get - getter
 * map - chainable
 * filter - chainable
 * forEach
 * reduce
 * reduceArr - chainable
 * push - chainable
 * merge - chainable
 * reindex - chainable
 * every
 * reveal - chainable
 * sort - chainable
 * useKeys - chainable
 */
namespace SimplePHP\SimpleData;

use SimplePHP\SimpleServer\SimpleErrorHandler;
use SimplePHP\Exception\ThrownException;

/**
 * @method  get - get the array
 * @method  map - chainable
 * @method  filter - chainable
 * @method  forEach
 * @method  reduce
 * @method  reduceArr - chainable
 * @method  push - chainable
 * @method  merge - chainable
 * @method  reindex - chainable
 * @method  every
 * @method  reveal - chainable
 * @method  sort - chainable
 * @method  useKeys - chainable
 */
class SimpleArray extends SimpleErrorHandler implements \ArrayAccess, \JsonSerializable {

  private $array;

  function __construct($array = []) {
    $this->array = (array) $array;
  }

  /**
   * Getter,
   * Gets the array and completes the chain. Cannot be chained further,
   * If there is fault in the chain, returns the GeneralFault object
   * @return  array|GeneralFault
   */
  public function get() {
    try {
      
      if ($this->hasFault) return $this->hasFault;
      
      // check $this->array incase Fault was appended to it via one of the 
      // methods rather than thrown from within
      if (array_key_exists('hasFault', $this->array))
      throw new ThrownException(
        'Fault in array ('.
        json_encode($this->array['hasFault']).
        ')'
      );

      // return the array
      return $this->array;

    } catch (\Exception $e) {
      return $this->stop($e);
    }
  }

  /**
   * Getter magic methods 
   */

  public function __serialize(): array {
    return $this->get();
  }

  public function __toString() {
    $count = count($this->array);
    return get_class($this)."({$count})";
  }

  /**
   * JsonSerializable interface 
   */

  public function jsonSerialize() {
    return $this->__serialize();
  }

  /**
   * ArrayAccess interface 
   */

  public function offsetSet($offset, $value) {
    if (is_null($offset)) {
        $this->array[] = $value;
    } else {
        $this->array[$offset] = $value;
    }
  }

  public function offsetExists($offset) {
      return isset($this->array[$offset]);
  }

  public function offsetUnset($offset) {
      unset($this->array[$offset]);
  }

  public function offsetGet($offset) {
      return isset($this->array[$offset]) ? $this->array[$offset] : null;
  }

  /**
   * Chainable array_map
   * @chainable
   * @param  function  $callback
   * ($value, $key)->$callback
   * @return  this
   */
  public function map($callback = null) {
    try {
      
      if ($this->hasFault) return;
      
      $keys = array_keys($this->array);
      $this->array = array_map($callback, $this->array, $keys);

    } catch (\Exception $e) {
      $this->stop($e);
    } finally {
      return $this;
    }
  }

  /**
   * Chainable wrapper for array_filter
   * @chainable
   * @param  function  $callback
   * @param  default  $flag - ($value)->$callback
   * @param  string  $flag = ARRAY_FILTER_USE_KEY - ($key)->callback
   * @param  string  $flag = ARRAY_FILTER_USE_BOTH - ($value, $key)->$callback
   * @return  this
   */
  public function filter($callback = null, $flag = 0) {
    try {
      
      if ($this->hasFault) return;
      
      $this->array = array_filter($this->array, $callback, $flag);

    } catch (\Exception $e) {
      $this->stop($e);
    } finally {
      return $this;
    }
  }

  /**
   * Wrapper for array_walk, to function like Array.forEach in Javascript
   * Cannot be chained further
   * @param  function  $callback
   * @param  any  $param
   * ($value, $key, $param)->$callback
   * @return  true|GeneralFault
   */
  public function forEach($callback = null, $param = null) {

    if ($this->hasFault) return $this->hasFault;

    array_walk($this->array, $callback, $param);

    return true;
  }

  /**
   * Wrapper for array_walk, functions like forEach, except in chunks as
   * defined by $chunkSize, using array_chunk
   * Cannot be chained further
   * @param  int  $chunkSize
   * @param  function  $callback
   * @param  any  $param
   * ($value, $param)->$callback
   * @return  true|GeneralFault
   */
  public function forEachChunk(
    Int $chunkSize = -1, 
    $callback = null, 
    $param = null
  ) {
    try {
      if ($this->hasFault) return $this->hasFault;

      // handle chunks
      (new SimpleArray(array_chunk($this->array, $chunkSize, true)))
      ->forEach(
        function($array) use ($callback, $param) {
          $callback($array, $param);
        }
      );
      return true;

    } catch (\Exception $e) {
      return $this->stop($e);
    }
  }

  /**
   * Wrapper for array_reduce
   * Cannot be chained further
   * @param  function  $callback
   * @param  any  $initial
   * ($carry||$initial, $value)->$callback
   * @return  mixed|GeneralFault
   */
  public function reduce($callback = null, $initial = null) {
    try {
      if ($this->hasFault) return $this->hasFault;

      return array_reduce($this->array, $callback, $initial);

    } catch (\Exception $e) {
      return $this->stop($e);
    }
  }

  /**
   * Chainable reducer function whereby the initial value is an array
   * Purpose is to function like a pivot table style array manipulation
   * @chainable
   * @param  function  $callback
   * @param  array  $initial - aka 'carry'
   * ($initial, $value)->$callback
   * @return  this
   */
  public function reduceArr($callback = null, $initial = []) {
    try {
      
      if ($this->hasFault) return;
      
      for ($i = 0; $i < count($this->array); $i++) {
        $initial = $callback($initial, $this->array[$i]);
      }
      $this->array = $initial;

    } catch (\Exception $e) {
      $this->stop($e);
    } finally {
      return $this;
    }
  }

  /**
   * Chainable wrapper for array_push
   * @chainable
   * @param  mixed  $element
   * @param  string  $key
   * @return  this
   */
  public function push($element, String $key = null) {
    try {
      
      if ($this->hasFault) return;
      
      if ($key) {
        $this->array[$key] = $element;
      } else {
        array_push($this->array, $element);
      }

    } catch (\Exception $e) {
      $this->stop($e);
    } finally {
      return $this;
    }
  }

  /**
   * Chainable wrapper for array_merge
   * @chainable
   * @param  array  $element
   * @return  this
   */
  public function merge(Array $array) {
    try {
      
      if ($this->hasFault) return;
      
      $this->array = array_merge($this->array, $array);

    } catch (\Exception $e) {
      $this->stop($e);
    } finally {
      return $this;
    }
  }

  /**
   * Strips null values and reindexes the array keys
   * @chainable
   * @param  bool  $avoidZero - shifts the array index up by ones
   * @return  this
   */
  public function reindex(
    Bool $avoidZero = false
  ) {
    try {
      
      if ($this->hasFault) return;
      
      // strip away null values
      $this->array = array_filter(
        $this->array, function($element){return !is_null($element);} 
      );

      // shifts array index by one, so starts at 1 rather than 0
      if ($avoidZero) {
        array_unshift($this->array,"");
        unset($this->array[0]);
      }
      
      // reindex
      $this->array = array_values($this->array);

    } catch (\Exception $e) {
      $this->stop($e);
    } finally {
      return $this;
    }
  }

  /**
   * Tests whether all elements in the array pass the test implemented by 
   * the provided function. Behaves like in JS.
   * End of chain.
   * @param  function  $callback(element, index, array)
   * @return  bool|GeneralFault
   */
  public function every($callback = null) {
    try {
      if ($this->hasFault) return $this->hasFault;

      // assume success
      $result = true;

      foreach($this->array as $key => $value) {
        $test = $callback($value, $key, $this->array);
        if (!$test) {
          // if there is one falsy test, return false and stop testing
          $result = false;
          break;
        }
      }

      return $result;

    } catch (\Exception $e) {
      return $this->stop($e);
    }
  }

  /**
   * Applies the full array as a param to the callback function
   * so the callback function can run 'readonly' functions
   * @chainable
   * @param  function  $callback(element, index, array)
   * ($this->array)->$callback
   * @return  this
   */
  public function reveal($callback = null) {
    try {
      if ($this->hasFault) return;

      // handle callback
      if (is_null($callback)) 
      throw new ThrownException('SimpleArray::reveal callback is not defined');

      // apply array to callback
      $callback($this->array);

    } catch (\Exception $e) {
      $this->stop($e);
    } finally {
      return $this;
    }
  }

  /**
   * Chainable wrapper for sort
   * @chainable
   * @param  string  $flag - optional sort_flag
   * @return  this
   * 
   * @example  $flag, type of
   * SORT_REGULAR, 
   * SORT_NUMERIC, 
   * SORT_STRING, 
   * SORT_LOCALE_STRING, 
   * SORT_NATURAL, 
   * SORT_FLAG_CASE
   */
  public function sort(Int $flag = 0) {
    try {
      // handle Fault in chain
      if ($this->hasFault) return;
      
      switch ($flag) {
        case 0;
          sort ($this->array, SORT_REGULAR);
        break;
        case 1;
          sort ($this->array, SORT_NUMERIC);
        break;
        case 2;
          sort ($this->array, SORT_STRING);
        break;
        case 3;
          sort ($this->array, SORT_LOCALE_STRING);
        break;
        case 4;
          sort ($this->array, SORT_NATURAL);
        break;
        case 5;
          sort ($this->array, SORT_FLAG_CASE);
        break;
      }

    } catch (\Exception $e) {
      $this->stop($e);
    } finally {
      return $this;
    }
  }

  /**
   * Chainable wrapper for array_keys, extracts keys from $this->array and
   * replaces it
   * @chainable
   * @return  this
   */
  public function useKeys() {
    try {
      if ($this->hasFault) return;

      $this->array = array_keys($this->array);

    } catch (\Exception $e) {
      $this->stop($e);
    } finally {
      return $this;
    }
  }

  /**
   * Works like javascript's array.findIndex 
   * 
   * @param  mixed  $needle  the search value to look for in the array  
   * @param  bool  $strict  whether to use strict type matching  
   * 
   * @return  int|string|false  the index value if found or false if not found  
   */
  public function findIndex($needle, bool $strict = true) {
    return array_search($needle, $this->array, $strict);
  }
}

?>