<?php 
/**
 * Simplifies MongoDB\Client and common actions 
 * @author  Johnson Zhou <johnson@simplyuseful.io>
 */
namespace SimplePHP\SimpleData;

use MongoDB\Client;

/**
 * @see  https://docs.mongodb.com/drivers/php
 * @see  https://docs.mongodb.com/php-library/current/reference/
 * 
 * @method  test
 */
class SimpleMongoDB extends Client {
  
  /**
   * @param  string  $uri 
   * - the uri to access the database  
   * - "mongodb://username:password@server:port"
   * - can be optionally set via php environment SIMPLE_PHP_MONGODB
   */
  public function __construct($uri = null) {
    $uri = $uri ?: getenv('SIMPLE_PHP_MONGODB');
    Client::__construct($uri);
  }

  /**
   * Test database connectivity by invoking Client::listDatabases() 
   * @return  bool  
   */
  public function test() {
    try {
      if ($this->listDatabases()) return true;
    } catch (\Exception $e) {
      return false;
    }
  }

}
 ?>