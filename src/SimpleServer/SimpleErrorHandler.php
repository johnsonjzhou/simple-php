<?php
/**
 * Error handling methods
 * 
 * Wraps functions around Bugsnag
 * A custom error handler to convert all notices, warnings, exceptions and
 * errors into ErrorException
 * Class can be Extended to take advantage of Fault handling capability
 * 
 * @author  Johnson Zhou (johnson@simplyuseful.io)
 * 
 * @class SimpleErrorHandler
 * 
 * @param  string  $_ENV('SIMPLE_PHP_VERSION') 
 * - app version, defaults to '1.0.0'  
 * 
 * @param  string  $_ENV('SIMPLE_PHP_ENV_MODE') 
 * - similar to process.env.NODE_ENV in node.js 
 * - carries value of 'development' or 'production', default is production  
 * 
 * @param  string  $_ENV('SIMPLE_PHP_BUGSNAG') 
 * - the apikey, if using Bugsnag
 * 
 * @protected
 * hasFault - false or holds error information
 * env_mode - production | development
 * 
 * @private
 * bugsnag
 * bugsnag_key
 * version
 * 
 * @method public static
 * errorHandler
 * 
 * @method public
 * stop
 * registerBugsnagUnhandled
 * 
 * @method private
 * initBugsnag
 * 
 * @method protected
 * breadcrumb
 * notify
 */

namespace SimplePHP\SimpleServer;

use Bugsnag\Client;
use Bugsnag\Handler;

use SimplePHP\Exception\GeneralFault;

use SimplePHP\Exception\OAuthRequired;
use SimplePHP\Resource\AuthURL;

use SimplePHP\Exception\FaultPassthrough;

use SimplePHP\Exception\ThrownException;

class SimpleErrorHandler {

  // whether we have a fault in the current process 
  // default is false
  // can be set to a FaultObject
  protected $hasFault = false;

  // the runtime environment as defined by $_ENV('SIMPLE_PHP_ENV_MODE') 
  // can be either 'development' or 'production' (default)
  // a status of 'development' provides more detail information
  // such as error stack trace 
  protected $env_mode;

  // bugsnag handler
  private $bugsnag;

  // bugsnag apikey as defined by $_ENV('SIMPLE_PHP_BUGSNAG') 
  private $bugsnag_key;

  // app version as defined by $_ENV('SIMPLE_PHP_VERSION') 
  private $version;
	
	public function __construct() {
    try {

      // assign environment variables

      $this->env_mode = 
        getenv('SIMPLE_PHP_ENV_MODE') ?: 'production';

      $this->bugsnag_key = 
        getenv('SIMPLE_PHP_BUGSNAG') ?: false;

      $this->version = 
        getenv('SIMPLE_PHP_VERSION') ?: '1.0.0';

      // initialise bugsnag if we have supplied the key  
      $this->bugsnag_key && $this->initBugsnag();
      
    } catch (\Exception $e) {
      $this->stop($e);
    }
  }

  /**
   * Initialises Bugsnag client
   */
  public function initBugsnag() {
    try {

      if (!$this->bugsnag_key) return;

      $this->bugsnag = Client::make($this->bugsnag_key);
      $this->bugsnag->setAppVersion($this->version);
      $this->bugsnag->setReleaseStage($this->env_mode);
      $this->bugsnag->setAppType('PHP Runtime');
      
    } catch (\Exception $e) {
      return $this->stop($e);
    }
  }

  /**
   * Activates $this->hasFault and attaches error message
   * Submits error message as breadcrumb to Bugsnag
   * Returns an error object
   * @return  GeneralFault|AuthURL  
   */
  public function stop(\Exception $e) {

    // assign 'hasFault' 
    // AuthURL implements GeneralFault by managing the httpCode and URL 
    $this->hasFault = ($e instanceof OAuthRequired) ? 
      (new AuthURL($e->getURL())) : new GeneralFault($e);

    // assign httpCode if this is a ThrownException
    // ThrownException can be extended and managed in a way
    // that considers http response message and status code
    // this is different from a standard \Exception
    ($e instanceof ThrownException) && 
    $this->hasFault instanceof GeneralFault && 
    $this->hasFault->setHttpCode($e->getCode());

    // notify bugsnag if thrown Exception is not a FaultPassthrough
    (!($e instanceof FaultPassthrough)) && $this->notify($e);

    return $this->hasFault;
  }

  /**
   * A custom error handler to convert all notices, warnings, exceptions and
   * errors into ErrorException
   */
  public static function errorHandler($severity, $message, $file, $line) {
    throw new \ErrorException($message, 0, $severity, $file, $line);
  }
  
  /**
   * @chainable
   * Registers the Bugsnag Handler
   * @return  this
   */
	public function registerBugsnagUnhandled() {
    try {

      ($this->bugsnag) && 
      ($this->bugsnag_key) && 
      Handler::register($this->bugsnag_key);

    } catch (\Exception $e) {
      $this->stop($e);
    } finally {
      return $this;
    }
	}

  /**
   * Sends a Bugsnag breadcrumb
   * @param  string  $message
   */
	protected function breadcrumb(String $message = '') {
    ($this->bugsnag) && 
    $this->bugsnag->leaveBreadcrumb($message);
	}

  /**
   * Sends a Bugsnag error notification
   * @param  \Exception  $e 
   */
	protected function notify(\Exception $e) {
    ($this->bugsnag) && 
    $this->bugsnag->notifyException($e);
  }

  public function __toString() {
    $string = 'Class SimpleErrorHandler';
    if ($this->hasFault) {
      $string .= "\n{$this->hasFault}";
    }
    return $string;
  }

}

?>