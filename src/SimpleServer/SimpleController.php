<?php
/**
 * Extensible Controller using SimpleErrorHandler
 * @author  Johnson Zhou (johnson@simplyuseful.io)
 * 
 * @class SimpleController
 * @uses  SimplePHP\SimpleServer\SimpleErrorHandler
 * 
 * @method  private
 * display
 * openUploadedFile
 * generateContentETag
 * 
 * @method  protected
 * request
 * getRequestContentJson
 * getRequestParam
 * getRequestFiles
 * getClientIp
 * getRequestInfo
 * setContent
 * respondJson
 * respondContent
 * 
 * @method  public
 * respondTemplate
 */

namespace SimplePHP\SimpleServer;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\File\UploadedFile;

use SimplePHP\SimpleServer\SimpleErrorHandler;
use SimplePHP\SimpleData\SimpleArray;
use SimplePHP\Resource\FileResource;
use SimplePHP\SimpleServer\SimpleMime; 

use SimplePHP\Exception\GeneralFault;
use SimplePHP\Exception\ThrownException;

/**
 * @method  request
 * @method  getRequestContentJson
 * @method  getRequestParam
 * @method  getRequestFiles
 * @method  getClientIp
 * @method  setContent
 * @method  respondTemplate
 * @method  respondJson
 * @method  respondContent
 */
class SimpleController extends SimpleErrorHandler {

  private $request;
  private $content;

  public function __construct() {
    $this->request = Request::createFromGlobals();
    SimpleErrorHandler::__construct();
  }

  // Request handlers ---------------------------------------------------------

  /**
   * Return the Request
   * @return Symfony\Component\HttpFoundation\Request
   */
  protected function request() {
    return $this->request;
  }

  /**
   * Takes a Symfony/../UploadedFile object,
   * get it's real path (usually in /tmp),
   * open using fopen()
   * @param  UploadedFile  $uploadedFile
   * @param  string  $mode - fopen modes, defaults to read only
   * @return  resource
   */
  private function openUploadedFile(
    UploadedFile $uploadedFile, 
    String $mode = 'r'
  ) {
    return fopen($uploadedFile->getRealPath(), $mode);
  }

  /**
   * Reads the Request content and parses it as a JSON object. 
   * A blank object will be returned if json_decode fails.  
   * 
   * @return  object  
   */
  protected function getRequestContentJson() {
    try {

      if ($this->hasFault) return;
    
      $json = $this->request->getContent();
      $parsed = json_decode($json);

      (gettype($parsed) !== 'object') && $parsed = (object) [];

      return $parsed;
    
    } catch (\Exception $e) {
      $this->stop($e);
    }
  }

  /**
   * @deprecated  use getRequestContentJson
   */
  protected function getJsonPostParams() {
    return $this->getRequestContentJson();
  }

  /**
   * Get a specific param from within the Request
   * Handles multipart/form-data, application/json
   * @return  mixed|null
   * 
   * @throws  ThrownException - 400, bad request
   */
  protected function getRequestParam(String $param) {
    try {

      if ($this->hasFault) return;

      $contentType = $this->request->headers->get('Content-Type');

      switch($contentType) {

        case (strrpos($contentType, 'application/json') !== false):
          $postParams = $this->getRequestContentJson();
          return (!property_exists($postParams, $param)) ? 
            null : $postParams->$param;
        
        case (strrpos($contentType, 'multipart/form-data') !== false):
          return $this->request->request->get($param);

        default:
          throw new ThrownException('Unsupported request content-type', 400);
      }

    } catch (\Exception $e) {
      $this->stop($e);
    }
  }

  /**
   * @deprecated  use getRequestParam
   */
  protected function getPostParam(String $param) {
    return $this->getRequestParam($param);
  }

  /**
   * Get an array of files (as FileResource) from a Request
   * Can be from regular Form submission or as part of FormData object
   * For this to work, content-type must be 'multipart/form-data'
   * 
   * @param  string  $param 
   *  - the request parameter containing files 
   *  - if using FormData object and multiple files, specify name as 'name[]'
   * 
   * @param  string  $allowedTypes - Regex of allowed MIME types if required
   *  - if this is specified, will throw InvalidPostBodyFault
   * 
   * @return  array  
   *  - of SimplePHP\Resource\FileResource
   * 
   * @see  https://github.com/symfony/symfony/blob/master/src/Symfony/Component/HttpFoundation/File/UploadedFile.php
   */
  protected function getRequestFiles(
    String $param, 
    String $allowedTypes = null
  ) {
    try {

      if ($this->hasFault) return;
      if (!$param === null) return [];

      // get request files 
      // as \Symfony\Component\HttpFoundation\File\UploadedFile 
      // this can return as one UploadedFile or array of UploadedFile(s)
      $files = $this->request->files->get($param);

      // if a single file, turn into an array
      if (gettype($files) !== 'array')
      $files = (new SimpleArray())->push($files)->get();

      return (new SimpleArray($files))
      ->map(function(UploadedFile $file) use ($allowedTypes) {
        // test for allowed mime types if specified
        // if does not conform to allowedTypes, remove from the array
        if (!is_null($allowedTypes)) {
          $mimeTester = (new SimpleMime())->setAllowedTypes($allowedTypes);
          if (
            ($mimeTester->isValid($file->getRealPath())) === 0
          )
          throw new ThrownException('Request file has invalid MIME type', 400);
        }
        
        return (new FileResource())
        ->setFilename($file->getClientOriginalName())
        ->setContents($this->openUploadedFile($file));
      })
      ->reindex()
      ->get();

    } catch (\Exception $e) {
      $this->stop($e);
    }
  }

  /**
   * @deprecated  use getRequestFiles
   */
  protected function getPostFiles(String $param) {
    return $this->getRequestFiles($param);
  }

  /**
   * @deprecated  use getRequestFiles
   */
  protected function getPostFilesResource(
    String $param, 
    String $allowedTypes = null
  ) {
    return $this->getRequestFiles($param, $allowedTypes);
  }

  /**
   * Returns the request IP address
   * 
   * @return  string|null
   */
  protected function getClientIp() {
    return $this->request->getClientIp();
  }

  /**
   * Returns the request Method and Path info
   * 
   * @return  string  
   */
  protected function getRequestInfo() {
    $method = $this->request->getRealMethod() ?: 'UNKNOWN-METHOD';
    $path = $this->request->getRequestUri() ?: 'UNKNOWN-URI';
    return $method. ' ' .$path;
  }

  // Response handlers --------------------------------------------------------

  /**
   * Displays a template with the varibles passed in from the controller
   * @param  string  $template - Path to the php template page to be used
   * @param  array  $parameters 
   * - Parameters to the passed into the page for rendering
   * 
   * @example  $parameters
   * array (
   *  'param' => 'foo'
   * )
   * $param will be available within $template
   * @return  string|HTML
   */
  private function display(
    String $template, 
    Array $parameters = []
  ) {
    try {
      
      if ($this->hasFault) return;
      
      ob_start();

      // check that $template file exists
      switch(file_exists($template)) {
        case true:
          // apply $parameters
          extract($parameters);
          // render the $template
          include_once $template;
          break;
        
        case false:
          // if $template does not exist, echo the text
          echo $template;
          break;
      }

      $output = ob_get_contents();
      ob_end_clean();

      return $output;
      
    } catch (\Exception $e) {
      $this->stop($e);
    }
  }

  /**
   * Generates a MD5 hash of a content string for the purposes of using
   * it as the ETag in the response header
   * @param  string  $content
   * 
   * @return  string
   */
  private function generateContentETag(String $content) {
    try {
      return md5($content);
    } catch (\Exception $e) {
      return '';
    }
  }

  /**
   * Sets the internal value of $content
   * @param  mixed  $content
   */
  protected function setContent($content) {
    $this->content = $content;
  }

  /**
   * Respond with a PHP template 
   * 
   * @param  string  $template 
   * - A raw string or path to the php template page to be used
   * @param  array  $parameters 
   * - Parameters to the passed into the page for rendering
   * @param  integer  $statusCode
   * - the HTTP response code, default is 200
   * @param  array  $headers
   * - array of headers, @see Symfony\Component\HttpFoundation\Response
   * 
   * @return  Response
   */
  public function respondTemplate(
    String $template, 
    Array $parameters = [], 
    Int $statusCode = 200, 
    Array $headers = []
  ) {
    try {

      // display the content or fault if exists
      $content = $this->hasFault ? 
        (string) $this->hasFault : $this->display($template, $parameters);
      
    } catch (\Exception $e) {
      $content = $e->getMessage();
      $statusCode = 500;
    } finally {
      $response = new Response($content, $statusCode, $headers);
      // deferred X-Frame-Options to server config. if not, enable here
      // $response->headers->set('X-Frame-Options', 'DENY');
      $response->prepare($this->request);
      $response->send();
      exit;
    }
  }

  /**
   * @deprecated  use respondTemplate
   */
  public function render(
    String $template, 
    Array $parameters = [], 
    Int $statusCode = 200, 
    Array $headers = []
  ) {
    $this->respondTemplate($template, $parameters, $statusCode, $headers);
  }

  /**
   * Respond with content as JSON
   * 
   * - format content for json render
   * - if content is not specified, use $this->content
   * 
   * @param  mixed  $content | $this->content
   * @param  int  $statusCode
   * @param  array  $headers
   * @return  Response
   */
  protected function respondJson(
    $content = null, 
    Int $statusCode = 200, 
    Array $headers = []
  ) {
    try {

      // handle null content
      // grab content from $this->content
      is_null($content) && ($content = $this->content);
      
      // handle existing Fault in Controller
      // render the existing Fault
      ($this->hasFault) && ($content = $this->hasFault);

      // apply the appropraite http statusCode if the
      // fault is an instance (or variant) of GeneralFault
      ($content instanceof GeneralFault) && ($statusCode = $content->httpCode);

      // attempt to encode the content as JSON
      $json = json_encode($content, JSON_NUMERIC_CHECK);
      
    } catch (\Exception $e) {
      $json = json_encode($this->stop($e), JSON_NUMERIC_CHECK);
      $statusCode = 500;
    } finally {
      $response = new Response(
        $json, 
        $statusCode, 
        array_merge(array('Content-Type' => 'application/json'), $headers)
      );
      // deferred X-Frame-Options to server config. if not, enable here
      // $response->headers->set('X-Frame-Options', 'DENY');
      $response->prepare($this->request);
      $response->send();
      exit;
    }
  }

  /**
   * @deprecated  use respondJson
   */
  protected function jsonRender(
    $content = null, 
    Int $statusCode = 200, 
    Array $headers = []
  ) {
    $this->respondJson($content, $statusCode, $headers);
  }

  /**
   * Respond with raw content based on provided content-type,
   * commonly used for text based returns, eg markdown
   * otherwise behaves similarly to respondJson
   * 
   * @param  mixed  $content | $this->content
   * @param  int  $statusCode - default 'text/plain'
   * @param  array  $headers
   * 
   * @return  Response
   */
  protected function respondContent(
    $content = null, 
    String $contentType = 'text/plain',
    Int $statusCode = 200, 
    Array $headers = []
  ) {
    try {

      // handle null content
      // grab content from $this->content
      is_null($content) && ($content = $this->content);
      
      // handle existing Fault in Controller
      // render the existing Fault
      ($this->hasFault) && ($content = $this->hasFault);

      // apply the appropraite http statusCode if the
      // fault is an instance (or variant) of GeneralFault
      ($content instanceof GeneralFault) && ($statusCode = $content->httpCode);

      // generate an ETag
      // check 'if-none-match' in the request
      // if both match, content has not changed
      // save bandwidth by sending blank response with status 304 Not Modified
      $ETag = $this->generateContentETag($content);
      $ifNoneMatch = $this->request->headers->get('if-none-match');

      if ($ifNoneMatch === $ETag) {
        $content = '';
        $statusCode = 304;
      }
      
    } catch (\Exception $e) {
      $content = (string) $this->stop($e);
      $statusCode = 500;
    } finally {
      $response = new Response(
        $content,
        $statusCode,
        array_merge(array('Content-Type' => $contentType), $headers)
      );
      // deferred X-Frame-Options to server config. if not, enable here
      // $response->headers->set('X-Frame-Options', 'DENY');
      $response->headers->set('ETag', $ETag);
      $response->prepare($this->request);
      $response->send();
      exit;
    }
  }

  /**
   * @deprecated  use respondContent
   */
  protected function contentRender(
    $content = null, 
    String $contentType = 'text/plain',
    Int $statusCode = 200, 
    Array $headers = []
  ) {
    $this->respondContent($content, $contentType, $statusCode, $headers);
  }
}

?>