<?php
/**
 * Functions related to checking of file MIME types
 * @author  Johnson Zhou (johnson@simplyuseful.io)
 */
namespace SimplePHP\SimpleServer;

use SimplePHP\SimpleServer\SimpleErrorHandler;

/**
 * @method  setAllowedTypes
 * @method  isValid
 */
class SimpleMime extends SimpleErrorHandler {

  private $allowedTypes;

  public function __construct() {}

  /**
   * @param  string  $allowedTypes
   *  - a string containing allowed MIME file types, eg application/pdf
   * 
   * @return  this
   */
  public function setAllowedTypes(String $allowedTypes) {
    $this->allowedTypes = $allowedTypes;
    return $this;
  }

  /**
   * @param  string  $filename 
   *  - the real path and name of the file to check 
   *  - if using \Symfony\Component\HttpFoundation\File\UploadedFile, 
   *  - invoke the UploadedFile->getRealPath() method 
   * 
   * @return  int  1|0
   */
  public function isValid($filename) {
    // file -bi output is of 'application/pdf; charset=iso-8859-1'
    $fileTypeOutput = exec("file -bi {$filename}");
    $fileType = explode(';', $fileTypeOutput)[0];
    return preg_match($this->allowedTypes, $fileType);
  }
}
?>