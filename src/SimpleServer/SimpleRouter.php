<?php
/**
 * Chainable wrapper for AltoRouter
 * @author  Johnson Zhou (johnson@simplyuseful.io)
 * 
 * @class SimpleRouter
 * 
 * @method  private
 * renderError
 * render404
 * checkControllerValid
 * 
 * @method  public
 * declareControllers
 * set404
 * map
 * match
 * 
 */

namespace SimplePHP\SimpleServer;

use AltoRouter;

use SimplePHP\SimpleServer\SimpleErrorHandler;
use SimplePHP\SimpleServer\SimpleController;
use SimplePHP\Exception\ThrownException;

/**
 * @param  string  $basepath
 * 
 * @method  declareControllers
 * @method  set404
 * @method  map
 * @method  match
 */
class SimpleRouter extends SimpleErrorHandler {

  private $controller;
  private $controllers;
  private $router;
  private $path404;

  public function __construct(String $basepath = '') {
    try {
      
      // declare router
      $this->router = new AltoRouter();

      // set basepath
      $this->router->setBasePath($basepath);

      // declare controller
      $this->controller = new SimpleController();

      // init $this->controllers
      $this->controllers = (object) [];
      
    } catch (\Exception $e) {
      $this->stop($e);
    }
  }

  /**
   * Respond with a fault, if it exists
   * @return  Symfony\Component\HttpFoundation\Response
   */
  private function respondError() {
    return $this->controller->respondTemplate(
      $this->hasFault ?: '500: UNDEFINED SERVER ERROR', [], 500
    );
  }

  /**
   * @deprecated  use respondError
   */
  private function renderError() {
    return $this->respondError();
  }

  /**
   * Respond with a 404 page 
   * @return  Symfony\Component\HttpFoundation\Response
   */
  private function respond404() {
    return $this->controller->respondTemplate(
      $this->path404 ?: '404: THIS PAGE COULD NOT BE FOUND', [], 404);
  }

  /**
   * @deprecated  use respond404
   */
  private function render404() {
    return $this->respond404();
  }

  /**
   * Checks controller mapping and declaration
   * @return  bool
   */
  private function checkControllerValid($controller) {
    try {
      
      if ($this->hasFault) return;
      
      if (
        (gettype($controller) !== 'array') ||
        (count($controller) !== 2) ||
        (!array_key_exists(0, $controller)) ||
        (!array_key_exists(1, $controller)) ||
        (gettype($controller[0]) !== 'string') ||
        (gettype($controller[1]) !== 'string')
      )
      throw new ThrownException('Controller incorrectly mapped');

      // handle controller declaration
      $class = $controller[0];
      if (
        (!property_exists($this->controllers, $controller[0])) ||
        (gettype($this->controllers->$class) !== 'object')
      )
      throw new ThrownException('Controller not yet declared');

      return true;
    } catch (\Exception $e) {
      $this->stop($e);
      return false;
    }
  }

  /**
   * Declare controllers using an array
   * @chainable
   * @param  array  $controllers - [name => class]
   * @example  $controllers
   * array(
   *  'test' => new Test()
   * )
   */
  public function declareControllers(Array $controllers = []) {
    try {
      
      if ($this->hasFault) return;
      
      $this->controllers = (object) $controllers;
      
    } catch (\Exception $e) {
      $this->stop($e);
    } finally {
      return $this;
    }
  }

  /**
   * Wrapper for AltoRouter::map
   * @chainable
   * @see https://altorouter.com/usage/mapping-routes.html
   * @param  string  $method - eg GET, POST, UPDATE, etc
   * @param  string  $route - eg /api/test
   * @param  mixed  $target
   * 
   * @example  $target as string
   * PATH
   * 
   * @example $target as array
   * [class(string), function(string)]
   */
  public function map(
    String $method,
    String $route,
    $target
  ) {
    try {
      
      if ($this->hasFault) return;

      // create the map
      $this->router->map(
        $method,
        $route,
        $target
      );
      
    } catch (\Exception $e) {
      $this->stop($e);
    } finally {
      return $this;
    }
  }

  /**
   * Sets the 404 page path
   * @chainable
   * @param  string  $path
   * 
   * @return  this
   */
  public function set404(String $path = null) {
    try {
      
      if ($this->hasFault) return;

      // set the 404 path only if $path is defined 
      // and the $path file exists
      $path && $this->path404 = file_exists($path) ? $path : $this->path404;
      
    } catch (\Exception $e) {
      $this->stop($e);
    } finally {
      return $this;
    }
  }

  /**
   * Getter,
   * matches the router call and returns the appropriate target
   * if route is not found, load '404.php' in BASEPATH
   */
  public function match() {
    try {
      
      // handle Fault
      if ($this->hasFault) return;

      // handle match
      // $match['params'] gets applied via call_user_func as an object
      $match = $this->router->match();

      // NO MATCH, 
      // respond with 404
      if ($match === false) {
        return $this->respond404();
      }

      // MATCH EXISTS
      // handle target
      if (!array_key_exists('target', $match)) 
      throw new ThrownException('Router target not specified');

      $target = $match['target'];

      // handle routerParams, eg /path/to/[:name]
      // apply $match['params'] as an object 
      $routerParams = array_key_exists('params', $match) ?
      (object) $match['params'] : null;

      // @example  $target
      // [class, function]
      switch (gettype($target) === 'array') {
        case true:

          $controller = $target;

          // handle controller is correctly mapped
          // pass on validity check Fault messages
          if (!$this->checkControllerValid($controller))
          return $this->respondError();

          // map the declared controller to the controller name
          $class = $controller[0];
          $controller[0] = $this->controllers->$class;

          call_user_func($controller, $routerParams);

          break;

        case false:

          // target is not an array
          // static route to a php page
          // check if static page exists, else throw error
          if (!file_exists($target))
          throw new ThrownException('Target file does not exist');

          return $this->controller->respondTemplate($target);
      }
      
    } catch (\Exception $e) {
      $this->stop($e);
      // respond with an error 
      return $this->respondError();
    }
  }
}

?>