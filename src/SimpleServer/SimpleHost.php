<?php
/**
 * Checks a remote host and/or port to see if its up or down
 * @author  Johnson Zhou (johnson@simplyuseful.io)
 */
namespace SimplePHP\SimpleServer;

class SimpleHost {
  
  public function __construct() {}

  /**
   * Check whether a certain host is available, using fsockopen
   * @param  string  $host
   * @param  int  $int
   * 
   * @return  bool
   */
  public static function available(
    String $host,
    Int $port = null
  ) {
    try {
      $available = false;
      $connection = @fsockopen($host, $port);
      if (is_resource($connection)) {
        $available = true;
        fclose($connection);
      }
    } catch (\Exception $e) {

    } finally {
      return $available;
    }
  }
}
?>