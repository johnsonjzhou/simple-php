<?php
/**
 * Methods for Minfos Script Service
 * @author Johnson Zhou (johnson@simplyuseful.io)
 */
namespace SimplePHP\SimpleIntegrations\SimpleMinfos;

use SimplePHP\SimpleIntegrations\SimpleMinfos\MinfosClient;
use SimplePHP\Resource\MinfosRequest as Request;
use SimplePHP\Resource\MinfosRequestNode as Node;
use SimplePHP\Resource\MinfosDataScript as ScriptData;
use SimplePHP\Resource\MinfosDataIdentifier as ScriptIdentifier;
use SimplePHP\Resource\MinfosDateTime;
use SimplePHP\SimpleData\SimpleArray;

use SimplePHP\Exception\ThrownException;

/**
 * @method  public  getScripts()  
 * @method  public  searchForScriptsByDispenseDate()  
 * @method  public  searchForScriptsSinceTag()  
 * @method  public  searchForScriptsByCustomerCodes()  
 * @method  public  queueRepeat()  
 * 
 * @todo  searchForScriptsByPrescriber  
 */
class MinfosScript extends MinfosClient {

  /**
   * @param  string  $serverPath  location of the Minfos Server  
   * @param  mixed  $params  any other params to pass to MinfosClient  
   * 
   * @see  SimplePHP\SimpleIntegrations\SimpleMinfos\MinfosClient
   */
  public function __construct(string $serverPath = '127.0.0.1:4434', ...$params) {
    $server = "http://{$serverPath}/script?wsdl";
    MinfosClient::__construct($server, ...$params);
  }

  /**
   * Return records for all requested scripts.  
   * 
   * The associated API method has a maximum return size of 500, therefore 
   * for larger calls, we are breaking up the calls into chunks and 
   * merging them before returning the entire data array.  
   * 
   * @param  array  $scriptCodes  
   *  array of script codes as integers. 
   *  note this is the Minfos database unique identifier, not the script number  
   * 
   * @return  array  of SimplePHP\Resource\MinfosDataScript    
   */
  public function getScripts(array $scriptCodes = []) {

    // handle customerCodes
    if (count($scriptCodes) < 1) 
      throw new ThrownException('Script code array cannot be empty.');

    // array of scripts to append to 
    $fetchedScripts = new SimpleArray();

    // perform the API calls in chunks of 500 
    (new SimpleArray($scriptCodes))
    ->forEachChunk(500, function($scriptCodesChunk) use (&$fetchedScripts) {

      $requestData = [
        new Node('ScriptCodes', 
          (new SimpleArray($scriptCodesChunk))
          ->map(function($code) { 
            return new Node('int', $code, Node::NAMESPACE_ARRAY);
          })
          ->get()
        , Node::NAMESPACE_MIDAS_SCRIPT)
      ];

      $request = new Request('request', $requestData);

      $response = $this->call('GetScripts', [ $request ], [
        'getElements' => [ 'GetScriptsResult', 'Script' ], 
        'asArray' => true
      ]);

      $fetchedScriptsChunk = (new SimpleArray($response))
      ->map(function($script) { return new ScriptData($script); })
      ->get();

      $fetchedScripts->merge($fetchedScriptsChunk);

    });

    return $fetchedScripts->get();
  }

  /**
   * Queue repeat for quick dispensing  
   * 
   * @param  int  $mnpn  Product MNPN  
   * @param  int  $scriptCode  Script unique identifier  
   * 
   * @return  bool 
   */
  public function queueRepeat(
    int $mnpn, 
    int $scriptCode 
  ) {

    $requestData = [
      new Node('Mnpn', $mnpn, Node::NAMESPACE_MIDAS_SCRIPT), 
      new Node('ScriptCode', $scriptCode, Node::NAMESPACE_MIDAS_SCRIPT), 
    ];

    $request = new Request('request', $requestData);

    try {
      $this->call('QueueRepeat', [ $request ]);
      return true;
    } catch (\SoapFault $e) {
      return false;
    }
  }

  /** 
   * Get a list of scripts for the given date range.  
   * 
   * @param  string  $fromDate  any string that can be read by DateTime  
   * @param  string  $toDate  any string that can be read by DateTime
   * @param  bool  $restrictToAvailableRepeats  
   * @param  bool  $restrictToScriptsOwing  
   * 
   * @return  array  of SimplePHP\Resource\MinfosDataScript    
   */
  public function searchForScriptsByCustomerCodes(
    array $customerCodes = [], 
    string $fromDate = null, 
    string $toDate = null, 
    int $tag = null, 
    bool $restrictToAvailableRepeats = false, 
    bool $restrictToScriptsOwing = false 
  ) {

    $fromDate && $fromDate = new MinfosDateTime($fromDate);
    $toDate && $toDate = new MinfosDateTime($toDate);
    
    $requestData = (new SimpleArray([

      new Node('CustomerCodes', 
        (new SimpleArray($customerCodes))
        ->map(function($code) { 
          return new Node('int', $code, Node::NAMESPACE_ARRAY);
        })
        ->get()
      , Node::NAMESPACE_MIDAS_SCRIPT), 

      $fromDate ? 
      new Node('FromDateTime', 
        $fromDate->getDate(), Node::NAMESPACE_MIDAS_SCRIPT) : null,  

      new Node('RestrictToAvailableRepeats', 
        $restrictToAvailableRepeats ? "true" : "false", Node::NAMESPACE_MIDAS_SCRIPT), 

      new Node('RestrictToScriptsOwing', 
        $restrictToScriptsOwing ? "true" : "false", Node::NAMESPACE_MIDAS_SCRIPT), 

      $tag ? 
      new Node('Tag', $tag, Node::NAMESPACE_MIDAS_SCRIPT) : null, 

      $toDate ? 
      new Node('ToDateTime', 
        $toDate->getDate(), Node::NAMESPACE_MIDAS_SCRIPT) : null 

    ]))
    ->filter(function($node) { return !is_null($node); })
    ->get();

    $request = new Request('request', $requestData);

    $response = $this->call('SearchForScriptsByCustomerCodes', [ $request ], [
      'getElements' => [ 'SearchForScriptsByCustomerCodesResult', 'ScriptIdentifier' ], 
      'asArray' => true
    ]);

    $scriptsIdentified = (new SimpleArray($response))
    ->map(function($identifier) { return new ScriptIdentifier($identifier); })
    ->map(function($identifier) { return $identifier->Code; })
    ->get();

    return $this->getScripts($scriptsIdentified);
  }

  /** 
   * Get a list of scripts for the given date range.  
   * 
   * @param  string  $fromDate  any string that can be read by DateTime  
   * @param  string  $toDate  any string that can be read by DateTime
   * @param  bool  $restrictToAvailableRepeats  
   * @param  bool  $restrictToScriptsOwing  
   * 
   * @return  array  of SimplePHP\Resource\MinfosDataScript    
   */
  public function searchForScriptsByDispenseDate(
    string $fromDate, 
    string $toDate, 
    bool $restrictToAvailableRepeats = false, 
    bool $restrictToScriptsOwing = false 
  ) {

    $fromDate && $fromDate = new MinfosDateTime($fromDate);
    $toDate && $toDate = new MinfosDateTime($toDate);
    
    $requestData = [
      new Node('FromDateTime', $fromDate->getDate(), Node::NAMESPACE_MIDAS_SCRIPT), 

      new Node('RestrictToAvailableRepeats', 
        $restrictToAvailableRepeats ? "true" : "false", Node::NAMESPACE_MIDAS_SCRIPT), 

      new Node('RestrictToScriptsOwing', 
        $restrictToScriptsOwing ? "true" : "false", Node::NAMESPACE_MIDAS_SCRIPT), 

      new Node('ToDateTime', 
        $toDate->getDate(), Node::NAMESPACE_MIDAS_SCRIPT), 
    ];

    $request = new Request('request', $requestData);

    $response = $this->call('SearchForScriptsByDispenseDate', [ $request ], [
      'getElements' => [ 'SearchForScriptsByDispenseDateResult', 'ScriptIdentifier' ], 
      'asArray' => true
    ]);

    $scriptsIdentified = (new SimpleArray($response))
    ->map(function($identifier) { return new ScriptIdentifier($identifier); })
    ->map(function($identifier) { return $identifier->Code; })
    ->get();

    return $this->getScripts($scriptsIdentified);
  }

  /**
   * Search for all scripts that were updated since the requested tag  
   * 
   * @param  int  $tag  
   * @param  bool  $restrictToExternallyPackedOnly  
   * 
   * @return  array  of SimplePHP\Resource\MinfosDataScript  
   */
  public function searchForScriptsSinceTag(
    int $tag, 
    bool $restrictToExternallyPackedOnly = false 
  ) {

    $requestData = [
      new Node('RestrictToExternallyPackedOnly', 
        $restrictToExternallyPackedOnly, Node::NAMESPACE_MIDAS_SCRIPT), 

      new Node('Tag', $tag, Node::NAMESPACE_MIDAS_SCRIPT)
    ];

    $request = new Request('request', $requestData);

    $response = $this->call('SearchForScriptsSinceTag', [ $request ], [
      'getElements' => [ 'SearchForScriptsSinceTagResult', 'ScriptIdentifier' ], 
      'asArray' => true
    ]);

    $scriptsIdentified = (new SimpleArray($response))
    ->map(function($identifier) { return new ScriptIdentifier($identifier); })
    ->map(function($identifier) { return $identifier->Code; })
    ->get();

    return $this->getScripts($scriptsIdentified);
  }

}
?>