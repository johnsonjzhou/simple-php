<?php
/**
 * Hybrid methods for the Minfos API that uses one or more API services to 
 * achieve useful purposes. 
 * @author Johnson Zhou (johnson@simplyuseful.io)
 */
namespace SimplePHP\SimpleIntegrations\SimpleMinfos;

use SimplePHP\SimpleIntegrations\SimpleMinfos\MinfosCustomer;
use SimplePHP\SimpleIntegrations\SimpleMinfos\MinfosScript;
use SimplePHP\Resource\MinfosDateTime;
use SimplePHP\SimpleData\SimpleArray;

use SimplePHP\Exception\ThrownException;

/**
 * @method  public  searchForScriptsByDispensedProduct  
 * @method  public  searchForCustomersByDispensedProduct  
 */
class MinfosExtended extends MinfosClient {

  /** @var  array */
  private $credentials;

  /**
   * @param  string  $serverPath  location of the Minfos Server  
   * @param  mixed  $params  any other params to pass to MinfosClient  
   * 
   * @see  SimplePHP\SimpleIntegrations\SimpleMinfos\MinfosClient
   */
  public function __construct(...$params) {
    $this->credentials = $params;
  }

  /** 
   * Return scripts dispensed with a given product 
   * and between a given date range.  
   * 
   * @param  int  $productCode  the Minfos Product Code  
   * @param  string  $fromDate  any string that can be read by DateTime  
   * @param  string  $toDate  any string that can be read by DateTime
   * 
   * @return  array  of SimplePHP\Resource\MinfosDataScript    
   */
  public function searchForScriptsByDispensedProduct(
    int $productCode, 
    string $fromDate, 
    string $toDate 
  ) {

    $fromDate = new MinfosDateTime($fromDate);
    $toDate = new MinfosDateTime($toDate);

    $scripts = (new MinfosScript(...$this->credentials))
    ->searchForScriptsByDispenseDate($fromDate, $toDate);

    return (new SimpleArray($scripts))
    ->filter(function($script) use ($productCode) {
      // SimplePHP\Resource\MinfosDataScript
      return $script->ProductCode === $productCode;
    })
    ->reindex()
    ->get();
  }

  /** 
   * Return scripts dispensed with a given product 
   * and between a given date range.  
   * 
   * @param  int  $productCode  the Minfos Product Code  
   * @param  string  $fromDate  any string that can be read by DateTime  
   * @param  string  $toDate  any string that can be read by DateTime
   * 
   * @return  array  of SimplePHP\Resource\MinfosDataCustomer  
   */
  public function searchForCustomersByDispensedProduct(
    int $productCode, 
    string $fromDate, 
    string $toDate 
  ) {
    $scripts = $this->searchForScriptsByDispensedProduct(
      $productCode, $fromDate, $toDate
    );

    $customerCodes = new SimpleArray();

    (new SimpleArray($scripts))
    ->forEach(function($script) use (&$customerCodes) {
      // SimplePHP\Resource\MinfosDataScript

      $customerCode = $script->CustomerCode;

      ($customerCodes->findIndex($customerCode) === false) && 
        $customerCodes->push($customerCode);
    });

    return (new MinfosCustomer(...$this->credentials))
    ->getCustomers($customerCodes->get()); 
  }

}
?>