<?php 
/**
 * @author  Johnson Zhou  <johnson@simplyuseful.io>  
 */

namespace SimplePHP\SimpleIntegrations\SimpleMinfos;

use SimplePHP\SimpleIntegrations\SimpleMinfos\MinfosCustomer;
use SimplePHP\SimpleIntegrations\SimpleMinfos\MinfosScript;
use SimplePHP\SimpleIntegrations\SimpleMinfos\MinfosExtended;
use SimplePHP\SimpleIntegrations\SimpleMinfos\MinfosBookings;

use SimplePHP\Exception\ThrownException;

class MinfosApi {

  /** @var  SimplePHP\SimpleIntegrations\SimpleMinfos\MinofsClient */
  private $service;

  /**
   * @param  string  $service  name of the Minfos API service to use  
   * @param  mixed  $params  params to pass onto the service constructor 
   * 
   * @see  SimplePHP\SimpleIntegrations\SimpleMinfos\MinofsClient  
   * 
   * @throws  SimplePHP\Exception\ThrownException  
   */
  public function __construct(string $service, ...$params) {
    switch (strtolower($service)) {
      case 'customer':
        return $this->service = new MinfosCustomer(...$params); 

      case 'script':
        return $this->service = new MinfosScript(...$params); 

      case 'extended':
        return $this->service = new MinfosExtended(...$params); 
      
      case 'bookings':
        return $this->service = new MinfosBookings(...$params); 

      default:
        throw new ThrownException("{$service} is not a valid Minfos API service");
    }
  }

  /**
   * Magic method to call a service method, checking that the method 
   * exists.
   * 
   * @return  mixed  output of the requested method  
   * 
   * @throws  SimplePHP\Exception\ThrownException
   */
  public function __call($name, $arguments) {
    if (!method_exists($this->service, $name))
      throw new ThrownException("Unknown service method {$name}");
    
    return $this->service->$name(...$arguments);
  }
}
?>