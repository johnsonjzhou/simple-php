<?php
/**
 * Methods for Minfos Bookings Service
 * @author Johnson Zhou (johnson@simplyuseful.io)
 */
namespace SimplePHP\SimpleIntegrations\SimpleMinfos;

use SimplePHP\SimpleIntegrations\SimpleMinfos\MinfosClient;
use SimplePHP\Resource\MinfosRequest as Request;
use SimplePHP\Resource\MinfosRequestNode as Node;
use SimplePHP\Resource\MinfosDateTime;
use SimplePHP\SimpleData\SimpleArray;

use SimplePHP\Exception\ThrownException;

/**
 * @method  public  createBooking()  
 * @method  public  cancelBooking()  
 */
class MinfosBookings extends MinfosClient {

  /**
   * @param  string  $serverPath  location of the Minfos Server  
   * @param  mixed  $params  any other params to pass to MinfosClient  
   * 
   * @see  SimplePHP\SimpleIntegrations\SimpleMinfos\MinfosClient
   */
  public function __construct(string $serverPath = '127.0.0.1:4434', ...$params) {
    $server = "http://{$serverPath}/bookings?wsdl";
    MinfosClient::__construct($server, ...$params);
  }

  /**
   * Creates a booking in the Minfos Bookings Queue
   * 
   * @param  array|object  $particulars  
   *  array of booking particulars
   * 
   * @param  string  $particulars['appointmentTime']  
   * @param  string  $particulars['bookingMadeTime'] optional  
   * @param  int     $particulars['bookingMadeOffset'] optional  
   * @param  string  $particulars['bookingNotes'] optional  
   * @param  string  $particulars['bookingReference'] optional, unique  
   * @param  string  $particulars['customerFirstname']   
   * @param  string  $particulars['customerSurname']   
   * @param  string  $particulars['dvaNumber'] optional  
   * @param  string  $particulars['dateOfBirth'] optional  
   * @param  string  $particulars['emailAddress'] requires either mobileNumber or emailAddress  
   * @param  string  $particulars['mnpn'] optional  
   * @param  string  $particulars['medicareCardNumber'] optional  
   * @param  string  $particulars['mobileNumber'] requires either mobileNumber or emailAddress    
   * @param  string  $particulars['postcode'] optional  
   * @param  string  $particulars['productName'] optional  
   * @param  string  $particulars['state'] optional  
   * @param  string  $particulars['streetAddress'] optional  
   * @param  string  $particulars['suburb'] optional  
   * 
   * @return  int  
   */
  public function createBooking($particulars = []) {

    // handle particulars
    if (gettype($particulars) === 'object') 
      $particulars = (array) $particulars;

    if (count($particulars) < 1) 
      throw new ThrownException('Booking particulars array cannot be empty.');

    // handle required particulars 
    if (!array_key_exists('appointmentTime', $particulars)) 
      throw new ThrownException('appointmentTime is required', 400);

    if (!array_key_exists('bookingReference', $particulars)) 
      throw new ThrownException('bookingReference is required', 400);
    
    if (!array_key_exists('customerFirstname', $particulars)) 
      throw new ThrownException('customerFirstname is required', 400);

    if (!array_key_exists('customerSurname', $particulars)) 
      throw new ThrownException('customerSurname is required', 400);

    if (!(
      array_key_exists('emailAddress', $particulars) || 
      array_key_exists('mobileNumber', $particulars)
    )) 
      throw new ThrownException('either emailAddress or mobileNumber is required', 400);

    // handle MinfosDateTime particulars
    if (array_key_exists('appointmentTime', $particulars))
      $particulars['appointmentTime'] = new MinfosDateTime($particulars['appointmentTime']);

    if (array_key_exists('bookingMadeTime', $particulars))
    $particulars['bookingMadeTime'] = new MinfosDateTime($particulars['bookingMadeTime']);

    // build request
    $requestData = (new SimpleArray([

      new Node('AppointmentTime', 
        $particulars['appointmentTime']->getMinfosTime(), Node::NAMESPACE_MIDAS_BOOKINGS),

      (array_key_exists('bookingMadeTime', $particulars) && 
        array_key_exists('bookingMadeOffset', $particulars)) ? 
      new Node('BookingMadeTime', [

        new Node('DateTime', 
          $particulars['bookingMadeTime']->getMinfosTime(), Node::NAMESPACE_MIDAS_SYSTEM),

        new Node('OffsetMinutes', 
          $particulars['bookingMadeOffset'], Node::NAMESPACE_MIDAS_SYSTEM),

      ], Node::NAMESPACE_MIDAS_BOOKINGS) : null, 

      array_key_exists('bookingNotes', $particulars) ? 
      new Node('BookingNotes', 
        $particulars['bookingNotes'], Node::NAMESPACE_MIDAS_BOOKINGS) : null, 

      new Node('BookingReference', 
        $particulars['bookingReference'], Node::NAMESPACE_MIDAS_BOOKINGS), 

      new Node('CustomerFirstname', 
        $particulars['customerFirstname'], Node::NAMESPACE_MIDAS_BOOKINGS), 

      new Node('CustomerSurname', 
        $particulars['customerSurname'], Node::NAMESPACE_MIDAS_BOOKINGS), 
    
      array_key_exists('dvaNumber', $particulars) ? 
      new Node('DVANumber', 
        $particulars['dvaNumber'], Node::NAMESPACE_MIDAS_BOOKINGS) : null, 

      array_key_exists('dateOfBirth', $particulars) ? 
      new Node('DateOfBirth', 
        $particulars['DateOfBirth'], Node::NAMESPACE_MIDAS_BOOKINGS) : null, 

      array_key_exists('emailAddress', $particulars) ? 
      new Node('EmailAddress', 
        $particulars['emailAddress'], Node::NAMESPACE_MIDAS_BOOKINGS) : null, 

      array_key_exists('mnpn', $particulars) ? 
      new Node('MNPN', 
        $particulars['mnpn'], Node::NAMESPACE_MIDAS_BOOKINGS) : null, 
      
      array_key_exists('medicareCardNumber', $particulars) ? 
      new Node('MedicareCardNumber', 
        $particulars['medicareCardNumber'], Node::NAMESPACE_MIDAS_BOOKINGS) : null, 

      array_key_exists('mobileNumber', $particulars) ? 
      new Node('MobileNumber', 
        $particulars['mobileNumber'], Node::NAMESPACE_MIDAS_BOOKINGS) : null, 

      array_key_exists('postcode', $particulars) ? 
      new Node('Postcode', 
        $particulars['postcode'], Node::NAMESPACE_MIDAS_BOOKINGS) : null, 

      array_key_exists('productName', $particulars) ? 
      new Node('ProductName', 
        $particulars['productName'], Node::NAMESPACE_MIDAS_BOOKINGS) : null, 

      array_key_exists('state', $particulars) ? 
      new Node('State', 
        $particulars['state'], Node::NAMESPACE_MIDAS_BOOKINGS) : null, 

      array_key_exists('streetAddress', $particulars) ? 
      new Node('StreetAddress', 
        $particulars['streetAddress'], Node::NAMESPACE_MIDAS_BOOKINGS) : null, 

      array_key_exists('suburb', $particulars) ? 
      new Node('Suburb', 
        $particulars['suburb'], Node::NAMESPACE_MIDAS_BOOKINGS) : null, 

    ]))
    ->filter(function($Node) { return !is_null($Node); })
    ->get();

    $request = new Request('booking', $requestData);

    $response = $this->call('CreateBooking', [ $request ], [
      'getElements' => [ 'CreateBookingResult' ]
    ]);

    return $response;
  }

  /**
   * Cancels a booking in the Minfos Bookings Queue
   * 
   * @param  string  $bookingReference  
   * 
   * @return  int  
   */
  public function cancelBooking(string $bookingReference) {

    $requestData = [

      new Node('BookingReference', 
        $bookingReference, Node::NAMESPACE_MIDAS_BOOKINGS),

    ];

    $request = new Request('booking', $requestData);

    $response = $this->call('CancelBooking', [ $request ], [
      'getElements' => [ 'CancelBookingResult' ]
    ]);

    return $response;
  }

}
?>