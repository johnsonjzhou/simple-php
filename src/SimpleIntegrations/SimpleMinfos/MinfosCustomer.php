<?php
/**
 * Methods for Minfos Customer Service
 * @author Johnson Zhou (johnson@simplyuseful.io)
 */
namespace SimplePHP\SimpleIntegrations\SimpleMinfos;

use SimplePHP\SimpleIntegrations\SimpleMinfos\MinfosClient;
use SimplePHP\Resource\MinfosRequest as Request;
use SimplePHP\Resource\MinfosRequestNode as Node;
use SimplePHP\Resource\MinfosDataCustomer as CustomerData;
use SimplePHP\Resource\MinfosDataIdentifier as CustomerIdentifier;
use SimplePHP\SimpleData\SimpleArray;

use SimplePHP\Exception\ThrownException;

/**
 * @method  public  getCustomers()  
 * @method  public  searchForCustomersByFacility()  
 * @method  public  searchForCustomersByName()
 * 
 * @todo  searchForCustomersSinceTag  
 */
class MinfosCustomer extends MinfosClient {

  /**
   * @param  string  $serverPath  location of the Minfos Server  
   * @param  mixed  $params  any other params to pass to MinfosClient  
   * 
   * @see  SimplePHP\SimpleIntegrations\SimpleMinfos\MinfosClient
   */
  public function __construct(string $serverPath = '127.0.0.1:4434', ...$params) {
    $server = "http://{$serverPath}/customer?wsdl";
    MinfosClient::__construct($server, ...$params);
  }

  /**
   * Return records for all requested customers.
   * 
   * @param  array  $customerCodes  array of customer codes as integers  
   * 
   * @return  array  of SimplePHP\Resource\MinfosDataCustomer  
   */
  public function getCustomers(array $customerCodes = []) {

    // handle customerCodes
    if (count($customerCodes) < 1) 
      throw new ThrownException('Customer code array cannot be empty.');

    $requestData = [
      new Node('CustomerCodes', 
        (new SimpleArray($customerCodes))
        ->map(function($code) { 
          return new Node('int', $code, Node::NAMESPACE_ARRAY);
        })
        ->get()
      , Node::NAMESPACE_MIDAS_CUSTOMER)
    ];

    $request = new Request('request', $requestData);

    $response = $this->call('GetCustomers', [ $request ], [
      'getElements' => [ 'GetCustomersResult', 'Customer' ], 
      'asArray' => true
    ]);

    return (new SimpleArray($response))
    ->map(function($customer) { return new CustomerData($customer); })
    ->get();
  }

  /**
   * Search for all customers that are part of the requested facility.
   * 
   * @param  int  $facilityCode  
   * @param  string  $facilityType  None | NursingHome | Hospital | ThirdParty  
   * 
   * @return  array  of SimplePHP\Resource\MinfosDataCustomerIdentifier  
   */
  public function searchForCustomersByFacility(
    int $facilityCode = null,
    string $facilityType = null
  ) {

    // both facilityCode and facilityType needs to be declared
    if (is_null($facilityCode) || is_null($facilityType)) {
      throw new ThrownException('Both facility Code and Type must be declared.');
    }

    // handle facilityType 
    $facilityTypes = ['None', 'NursingHome', 'Hospital', 'ThirdParty'];
    if (!array_search($facilityType, $facilityTypes))
      throw new ThrownException("{$facilityCode} is not a known facility type");

    $requestData = [
      new Node('FacilityKey', [
        new Node('Code', $facilityCode, Node::NAMESPACE_MIDAS_FACILITY), 
        new Node('Type', $facilityType, Node::NAMESPACE_MIDAS_FACILITY), 
      ], Node::NAMESPACE_MIDAS_CUSTOMER)
    ];

    $request = new Request('request', $requestData);

    $response = $this->call('SearchForCustomersByFacility', [ $request ], [
      'getElements' => [ 
        'SearchForCustomersByFacilityResult', 'CustomerIdentifier' 
      ], 
      'asArray' => true
    ]);

    return (new SimpleArray($response))
    ->map(function($customer) { return new CustomerIdentifier($customer); })
    ->get();
  }

  /**
   * Search for all customers with the requested name.
   * 
   * @param  array  $name  
   * @param  string  $name['lastname']  
   * @param  string  $name['firstname]  
   * 
   * @return  array  of SimplePHP\Resource\MinfosDataCustomerIdentifier  
   */
  public function searchForCustomersByName(array $name = []) {

    $requestData = [];

    if (array_key_exists('firstname', $name)) {
      array_push($requestData, 
        new Node('FirstName', $name['firstname'], Node::NAMESPACE_MIDAS_CUSTOMER)
      );
    }

    if (array_key_exists('lastname', $name)) {
      array_push($requestData, 
        new Node('LastName', $name['lastname'], Node::NAMESPACE_MIDAS_CUSTOMER)
      );
    }

    $request = new Request('request', $requestData);

    $response = $this->call('SearchForCustomersByName', [ $request ], [
      'getElements' => [ 
        'SearchForCustomersByNameResult', 'CustomerIdentifier' 
      ], 
      'asArray' => true
    ]);

    $customersIdentified = (new SimpleArray($response))
    ->map(function($customer) { return new CustomerIdentifier($customer); })
    ->map(function($identifier) { return $identifier->Code; })
    ->get();

    return $this->getCustomers($customersIdentified);
  }

}
?>