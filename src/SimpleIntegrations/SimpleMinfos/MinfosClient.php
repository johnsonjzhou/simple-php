<?php
/**
 * Extensible client to access a Minfos server endpoint with PHP's SoapClient  
 * @author Johnson Zhou (johnson@simplyuseful.io)
 */
namespace SimplePHP\SimpleIntegrations\SimpleMinfos;

use \SoapClient;

/**
 * @method  protected  initClient()  
 * @method  protected  call()  
 * @method  protected  getLastResponse()  
 */
class MinfosClient {

  private const SOAP_OPTIONS = [
    'soap_version'=> SOAP_1_1, 
    'exceptions'=> true, 
    'trace'=> 1, 
    'cache_wsdl'=> WSDL_CACHE_NONE 
  ];

  /** @var  \SoapClient */
  protected $client;

  /** @var  string */
  private $server;

  /** @var  array */
  private $credentials = [
    'login' => '', 
    'password' => ''
  ];

  /** @var  object */
  private $lastResponse;
  
  /**
   * @param  string  $server  URL of the Minfos Server (WSDL) service to access  
   * @param  string  $login  
   * @param  string  $password  
   */
  public function __construct(
    string $server, 
    string $login, 
    string $password  
  ) { 
    $this->server = $server;
    $this->credentials['login'] = $login;
    $this->credentials['password'] = $password;
  }

  /**
   * Initiates the SoapClient
   */
  protected function initClient() {
    $this->client = new \SoapClient(
      $this->server, 
      array_merge($this->credentials, self::SOAP_OPTIONS)
    );
  }

  /**
   * Initiates a __soapCall for a given method, applying request data 
   * 
   * @param  string  $methodName  
   *  name of the endpoint method  
   * 
   * @param  array  $parameters  
   *  array of parameters formatted as SoapVar. 
   *  best to create using SimplePHP\Resource\MinfosRequest.  
   * 
   * @param  array  $options  options on how to deliver the response  
   * 
   * @param  bool  $options['getElements']  
   *  for list based data, the API response will be an object for 
   *  singular data or an array of objects for a collection of data, 
   *  set this to true so that singular data will be wrapped in an array
   * 
   * @param  array  $options['asArray']    
   *  recursively access the response data with declared element names as 
   *  properties of the response object  
   * 
   * @return  object|array  the response from the call
   * 
   * @throws  \SoapFault  
   */
  protected function call(
    string $methodName, 
    array $parameters = [], 
    array $options = []
  ) {

    // initiate the client 
    if (!$this->client) $this->initClient();

    // call the method with SoapClient  
    $this->lastResponse = $this->client->__soapCall($methodName, $parameters); 

    // extract the data from the SoapClient response  
    $response = json_decode(json_encode($this->lastResponse));

    // if responseElements are declared, we will recursively access 
    // the response data to get the info we want 
    // at the same time, standardise 
    if (array_key_exists('getElements', $options) && is_array($options['getElements'])) {
      foreach ($options['getElements']   as $element) {
        $response = $response->$element;
      }
    }

    // standardise the response data so it's always an array  
    if (array_key_exists('asArray', $options) && $options['asArray'] === true)
      $response = is_array($response) ? $response : [ $response ];

    return $response;
  }

  /**
   * Returns the response object from the last call made from the client  
   * 
   * @return  object
   */
  protected function getLastResponse() {
    return $this->lastResponse;
  }
}

?>