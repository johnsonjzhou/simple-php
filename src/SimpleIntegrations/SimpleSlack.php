<?php
/**
 * Wrapper for the Slack Web Api
 * @author  Johnson Zhou (johnson@simplyuseful.io)
 * @see  https://api.slack.com/web
 * 
 * @method  public
 * test
 * postMessage
 * fileUpload
 * 
 * @note  response
 * If HTTP_TESTMODE_ENV is set to true, response will include full content
 * received from the Slack Web Api. Alternatively, response will only include
 * the "ok" property.
 */
namespace SimplePHP\SimpleIntegrations;

use GuzzleHttp\Client;
use SimplePHP\SimpleServer\SimpleErrorHandler;
use GuzzleHttp\Exception\ConnectException;
use SimplePHP\Exception\ThrownException;
use SimplePHP\Resource\FileResource;
use SimplePHP\Resource\GuzzleMultipartParam as MultipartParam;
use SimplePHP\SimpleData\SimpleArray;

/**
 * @method  test  
 * @method  postMessage  
 * @method  fileUpload  
 */
class SimpleSlack extends SimpleErrorHandler {

  /** @var  string */
  private $token;

  /** @var  GuzzleHttp\Client */
  private $httpClient;

  /** @var  bool */
  private $testMode;

  /**
   * @param  string  $token - the Slack API token  
   */
  public function __construct(String $token) {

    $this->token = $token;

    $this->httpClient = new Client([
      'base_uri' => 'https://slack.com/api/',
      'timeout' => 10,
    ]);
    
    $this->testMode = (getenv('SIMPLE_PHP_ENV_MODE') === 'development');
  }

  /**
   * Sends the request via httpClient, 
   * return the response from Slack Api if in test mode, or
   * return a sanitised response with only the "ok" key if in production, or
   * return a response with "ok" as false and "status" code if http error
   * @return  object - [ok ... ]
   */
  private function request(String $method, String $path, Array $options = []) {
    if ($this->hasFault) return;

    try {
      $response = $this->httpClient->request($method, $path, $options);
      $status = $response->getStatusCode();
    } catch (ConnectException $e) {
      // GuzzleHttp throws a ConnectException when timeout is exceeded
      // Set this as status 504 Gateway Timeout
      $status = 504;
    }

    if ($status === 200) {
      $apiResponse = json_decode($response->getBody()->getContents());
      return ($this->testMode)  ?
      $apiResponse : (object) ['ok' => $apiResponse->ok];
    } else {
      // http error, not status 200
      return (object) ['ok' => false, 'status' => $status];
    }
  }

  /**
   * Checks API calling code
   * @see  https://api.slack.com/methods/api.test
   */
  public function test() {
    try {
      if ($this->hasFault) return $this->hasFault;
      return $this->request('POST', 'api.test');
    } catch (\Exception $e) {
      return $this->stop($e);
    }
  }

  /**
   * Sends a message to a channel
   * @see  https://api.slack.com/methods/chat.postMessage
   * @see  https://api.slack.com/reference/surfaces/formatting#visual-styles
   */
  public function postMessage(String $channel, String $text) {
    try {
      if ($this->hasFault) return $this->hasFault;
      return $this->request('POST', 'chat.postMessage', [
        'form_params' => [
          'token' => $this->token,
          'channel' => $channel,
          'text' => $text
        ]
      ]);
    } catch (\Exception $e) {
      return $this->stop($e);
    }
  }

  /**
   * Uploads or creates file(s) along with a message
   * @see  https://api.slack.com/methods/files.upload
   * @see  http://docs.guzzlephp.org/en/stable/request-options.html#multipart
   * 
   * @param  string  $channel 
   *  - channel to post the file / message
   * @param  array  $file
   *  - array of FileResource
   * @param  string  $comment
   *  - this forms the initial_comment tag which translates to a message
   * 
   * @return  array  - of Slack API responses, [][ok ... ]
   * 
   * @throws  ThrownException  
   */
  public function fileUpload(
    String $channel,
    $files,
    $comment = ''
  ) {
    try {
      if ($this->hasFault) return $this->hasFault;

      $multipart = new SimpleArray();

      // handle token
      $multipart->push(
        (new MultipartParam('token', $this->token))->toArray()
      );

      // handle channel
      $multipart->push(
        (new MultipartParam('channels', $channel))->toArray()
      );

      // handle files and comment
      // files.upload API only accepts one file per request
      // submit comment with first request and first file
      // submit a new request for each subsequent file
      // add a file count - x of y
      // include comment with first request
      // return the array of responses back to the client

      $fileCount = count($files);
      if ($fileCount < 1)
      throw new ThrownException('No valid files provided to upload', 400);

      $requests = (new SimpleArray($files))
      ->map(function($file, $index) use ($multipart, $comment, $fileCount) {
        if (!($file instanceof FileResource)) return null;
        $pageNumber = $index+1;
        $request = (clone $multipart)
        ->push(
          (new MultipartParam(
            'file', $file->contents, [], $file->filename
          ))->toArray()
        )
        ->push(
          (new MultipartParam(
            'title', "{$pageNumber} of {$fileCount}"
          ))->toArray()
        );

        if ($index === 0) $request
        ->push(
          (new MultipartParam('initial_comment', $comment))->toArray()
        );

        return $request->get();
      })
      ->reindex()
      ->get();

      return (new SimpleArray($requests))
      ->map(function($request) {
        return $this->request('POST', 'files.upload', [
          'multipart' => $request
        ]);
      })
      ->get();

    } catch (\Exception $e) {
      return $this->stop($e);
    }
  }
}
?>