<?php
/**
 * Wrapper for the Pin Payments Web Api
 * @author  Johnson Zhou <johnson@simplyuseful.io>
 * @see  https://pinpayments.com/developers/api-reference
 * 
 * extended by:
 * SimplePHP\SimpleIntegrations\SimplePinPayments\Charge
 * 
 * @note base_uri
 * based on SIMPLE_PHP_ENV_MODE
 * test: test-api.pinpayments.com
 * live: api.pinpayments.com
 */
namespace SimplePHP\SimpleIntegrations;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\ClientException;
use SimplePHP\SimpleServer\SimpleErrorHandler;
use SimplePHP\Resource\PinPaymentsResponse;

/**
 * @method  request 
 */
class SimplePinPayments extends SimpleErrorHandler {

  /** @var  string */
  protected $token;

  /** @var  GuzzleHttp\Client */
  protected $httpClient;

  /** @var  bool */
  protected $testMode;

  /**
   * @param  string  $token - the authorisation token for Pin Payments  
   */
  public function __construct(String $token) {

    $this->token = $token;

    $this->testMode = (getenv('SIMPLE_PHP_ENV_MODE') === 'development');
    $base_uri = $this->testMode ? 
      'test-api.pinpayments.com' : 'api.pinpayments.com';

    $this->httpClient = new Client([
      'base_uri' => "https://{$base_uri}/",
      'timeout' => 10,
    ]);
    
  }

  /**
   * Sends the request via httpClient,  
   * Returns a standardised response object
   * 
   * @return  SimplePHP\Resource\PinPaymentsResponse
   */
  protected function request(
    String $method, 
    String $path, 
    Array $options = []
  ) {
    if ($this->hasFault) return;
    try {
      $auth = ['auth' => [$this->token, null]];
      $response = $this->httpClient->request(
        $method, $path, array_merge($auth, $options)
      );
      $status = $response->getStatusCode();
      $contents = json_decode($response->getBody()->getContents());
    } catch (ConnectException $e) {
      // GuzzleHttp throws a ConnectException when timeout is exceeded
      // Set this as status 504 Gateway Timeout
      $status = 504;
      $contents = (object) [
        'error' => 'timeout',
        'error_description' => 'Could not connect to payment gateway'
      ];
    } catch (ClientException $e) {
      // get the response from the error object
      // and pass through the status & contents
      $response = $e->getResponse();
      $status = $response->getStatusCode();
      $contents = json_decode($response->getBody()->getContents());
    } finally {
      // if we are in test mode, return full response
      // otherwise only return the status code
      // 24-May-2020, decided to return the full message regardless
      // as it will be very difficult to handle at the front end
      // if payment fails
      return new PinPaymentsResponse($contents, $status, $this->testMode);
    }
  }
}
?>