<?php
/**
 * Wrapper for common Google Drive functions
 * @author  Johnson Zhou (johnson@simplyuseful.io)
 * 
 * @see https://github.com/googleapis/google-api-php-client-services/tree/master/src/Google/Service/Drive
 * @see https://developers.google.com/resources/api-libraries/documentation/drive/v3/php/latest/index.html
 * @see https://developers.google.com/resources/api-libraries/documentation/drive/v3/php/latest/class-Google_Service_Drive_Files_Resource.html
 * 
 * @note file | folder is treated as the same within Google Drive, 
 * only difference is the mimeType property  
 */

namespace SimplePHP\SimpleIntegrations\SimpleGoogle;

use SimplePHP\SimpleIntegrations\SimpleGoogle\GoogleClient;
use SimplePHP\Exception\ThrownException;

use \Google_Service_Drive as Drive;
use \Google_Service_Drive_DriveFile as Files;
use \Google_Service_Exception;

/**
 * @method  public  setId()  
 * @method  public  getDruveFile()  
 * @method  public  download()  
 * @method  public  create()  
 * @method  public  listFolderContents()  
 */
class SimpleDrive extends GoogleClient {

  /** @var  \Google_Service_Drive */
  protected $drive;

  /** @var  string */
  protected $fileId;

  /** 
   * @param  mixed  $params  params that will be passed to GoogleClient  
   * @see  SimplePHP\SimpleIntegrations\SimpleGoogle\GoogleClient  
   */
  public function __construct(...$params) {
    GoogleClient::__construct(...$params);
  }

  /**
   * Initialises the Drive service
   * 
   * @throws  SimplePHP\Exception\OAuthRequired
   */
  protected function initDrive() {
    (!$this->client) && $this->initClient();
    $this->drive = new Drive($this->client);
  }

  /**
   * Validates stored metadata within $this->data,
   * make sure to run $this->getMeta() first,
   * checks for property 'mimeType' and matches it to the requested $mimeType
   * 
   * @param  string  $mimeType
   * @param  \Google_Service_Drive_DriveFile  $data  
   * 
   * @return  bool - true if matches
   * 
   * @throws  SimplePHP\Exception\ThrownException  
   */
  protected function validateMimeType(
    string $mimeType = null,
    \Google_Service_Drive_DriveFile $data 
  ) {
    // handle mimeType
    if (is_null($mimeType))
      throw new ThrownException('mimeType not defined');

    if (
      (gettype($data) !== 'object') ||
      (!property_exists($data, 'mimeType'))
    )
      throw new ThrownException('DriveFile data does not contain mimeType');

    return (stripos($data->mimeType, $mimeType) === false) ? false : true;
  }

  /**
   * Sets the fileId
   * @chainable
   * 
   * @param  string  $fileId
   * 
   * @return  this  
   * 
   * @throws  SimplePHP\Exception\ThrownException  
   */
  public function setId(String $fileId = null) {
    // handle fileId
    if (is_null($fileId))
      throw new ThrownException('File Id not defined');

    $this->fileId = $fileId;

    return $this;
  }

  /**
   * Fetches a single file or folder from Drive,
   * using $this->fileId and Files.get method
   * 
   * @param  array  $optParams
   * @see https://developers.google.com/drive/api/v3/reference/files/get
   * 
   * @default  
   * supportsAllDrives: true  
   * 
   * @return  \Google_Service_Drive_DriveFile  
   * 
   * @throws  SimplePHP\Exception\ThrownException  
   */
  public function getDriveFile(array $optParams = []) {
    try {

      if (!$this->drive) $this->initDrive();
      
      // handle fileId
      if (!$this->fileId)
        throw new ThrownException('File Id not defined');

      // handle params
      $params = ['supportsAllDrives' => true];

      return ($this->drive
        ->files
        ->get($this->fileId, array_merge($params, $optParams))
      );
      // Google_Service_Drive_DriveFile || 

    } catch (Google_Service_Exception $e) {
      throw new ThrownException($this->getServiceExceptionDetails($e), 502);
    } 
  }

  /**
   * Downloads a file based on fileId,
   * will not convert a Google Docs file to another format
   * 
   * @param  array  $optParams
   * @see https://developers.google.com/drive/api/v3/reference/files/get
   * 
   * @default
   * alt: media
   * supportsAllDrives: true
   * 
   * @return  resource  the raw contents of the file  
   * 
   * @throws  SimplePHP\Exception\ThrownException  
   */
  public function download(array $optParams = []) {
    try {

      if (!$this->drive) $this->initDrive();
      
      // handle fileId
      if (!$this->fileId)
        throw new ThrownException('File Id not defined');

      // handle params
      $params = [
        'supportsAllDrives' => true,
        // important for file download
        'alt' => 'media'
      ];

      return  ($this->drive
        ->files
        ->get($this->fileId, array_merge($params, $optParams))
      )
      // GuzzleHttp\Psr7\Response 
      ->getBody()
      // GuzzleHttp\Psr7\Stream
      ->getContents()
      ;

    } catch (Google_Service_Exception $e) {
      throw new ThrownException($this->getServiceExceptionDetails($e), 502);
    }
  }

  /**
   * Creates a new file in Google Drive
   * 
   * @param  mixed  $content  the content to create  
   * @param  array  $metadata  properties for Files resource
   * @param  array  $optParams  optional query parameters
   * @see https://developers.google.com/drive/api/v3/reference/files/create
   * 
   * @default
   * supportsAllDrives: true
   * uploadType: multipart
   * 
   * @return  string  google drive file id
   * 
   * @throws  SimplePHP\Exception\ThrownException  
   */
  public function create(
    $content = null,
    array $metadata = [],
    array $optParams = []
  ) {
    try {
      if (!$this->drive) $this->initDrive();

      $postBody = new Files($metadata);
      
      $defaultParams = [
        'data' => $content,
        'supportsAllDrives' => true,
        'uploadType' => 'multipart'
      ];

      $create = $this->drive->files
      ->create($postBody, array_merge($defaultParams, $optParams));

      if (
        is_null($create) ||
        gettype($create) !== 'object' ||
        !property_exists($create, 'id')
      )
        throw new ThrownException('Error in creating new Google Drive file');

      return $create->id;
      
    } catch (Google_Service_Exception $e) {
      throw new ThrownException($this->getServiceExceptionDetails($e), 502);
    } 
  }

  /**
   * Lists the contents of a folder,
   * using $this->fileId as the folder id and Files.list method
   * Check using getDriveFile() that the fileId indeed refers to a folder
   * 
   * @param  array $optParams
   * @see https://developers.google.com/drive/api/v3/reference/files/list
   * @see https://developers.google.com/drive/api/v3/search-files
   * 
   * @default
   * orderBy: modifiedTime
   * pageSize: 1000
   * includeItemsFromAllDrives: true
   * supportsAllDrives: true
   * 
   * @return  array  of files Resource 
   * @see  https://developers.google.com/drive/api/v3/reference/files#resource
   * 
   * @throws  SimplePHP\Exception\ThrownException  
   */
  public function listFolderContents(array $optParams = null) {
    try {

      if (!$this->drive) $this->initDrive();

      // handle fileId
      if (!$this->fileId)
        throw new ThrownException('File Id not defined');

      // check metadata and make sure mimeType is a folder
      $driveFile = $this->getDriveFile();
      if (!$this->validateMimeType('folder', $driveFile))
        throw new ThrownException("DriveFile id={$this->fileId} is not a folder");

      // handle optParams
      if (is_null($optParams))
      $optParams = [
        'orderBy' => 'modifiedTime',
        'pageSize' => 1000,
        'includeItemsFromAllDrives' => true,
        'supportsAllDrives' => true
      ];

      // param with the search query
      $param = [
        'q' => "'{$this->fileId}' in parents"
      ];

      $fileList = ($this->drive
        ->files->listFiles(array_merge($param, $optParams))
      );

      if (
        (gettype($fileList) === 'object') &&
        (property_exists($fileList, 'files'))
      ) {
        return $fileList->files;
        // array 
      } else {
        throw new ThrownException('Incorrect response format', 502);
      }

    } catch (Google_Service_Exception $e) {
      throw new ThrownException($this->getServiceExceptionDetails($e), 502);
    } 
  }
}
?>