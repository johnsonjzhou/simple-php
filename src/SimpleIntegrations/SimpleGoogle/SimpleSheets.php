<?php
/**
 * Wrapper for common Google Sheets functions
 * @author  Johnson Zhou (johnson@simplyuseful.io)
 * 
 * @see https://developers.google.com/sheets/api/reference/rest
 * @see https://developers.google.com/resources/api-libraries/documentation/sheets/v4/php/latest/index.html
 */

namespace SimplePHP\SimpleIntegrations\SimpleGoogle;

use SimplePHP\SimpleIntegrations\SimpleGoogle\GoogleClient;
use SimplePHP\SimpleData\SimpleArray;
use SimplePHP\Exception\ThrownException;

use \Google_Service_Sheets as Sheets;

/**
 * @method  protected  initSheets()  
 * 
 * @method  public  setSheet()  
 * @method  public  setRange()  
 * @method  public  fetchValues()  
 * @method  public  mapHeadings()  
 * @method  public  get()  
 * 
 * @example usage
 * (new SimpleSheets())  
 * ->setSheet(id)  
 * ->setRange(range)  
 * ->fetchValues()  
 * ->mapHeadings(null | [headings])  
 * ->get()  
 */
class SimpleSheets extends GoogleClient {

  /** @var  \Google_Service_Sheets */
  protected $sheets;

  /** @var  string */
  protected $spreadsheetId;

  /** @var  string */
  protected $range;

  /** @var  array */
  private $data;

  /** 
   * @param  mixed  $params  params that will be passed to GoogleClient  
   * @see  SimplePHP\SimpleIntegrations\SimpleGoogle\GoogleClient  
   */
  public function __construct(...$params) {
    GoogleClient::__construct(...$params);
  }

  /**
   * Initialises the Sheets service
   * 
   * @throws  SimplePHP\Exception\OAuthRequired
   */
  protected function initSheets() {
    (!$this->client) && $this->initClient();
    $this->sheets = new Sheets($this->client);
  }

  /**
   * Sets the spreadsheetId
   * @chainable
   * 
   * @param  string  $sheetId
   * 
   * @return  this  
   * 
   * @throws  SimplePHP\Exception\ThrownException  
   */
  public function setSheet(string $spreadsheetId = null) {
    // handle spreadsheetId
    if (is_null($spreadsheetId))
    throw new ThrownException('Spreadsheet Id not defined');

    $this->spreadsheetId = $spreadsheetId; 

    return $this;
  }

  /**
   * Sets the range to use
   * @chainable
   * 
   * @param  string  $range
   * 
   * @return  this  
   */
  public function setRange(string $range = null) {
    // handle range
    if (is_null($range))
      throw new ThrownException('Range is not defined');

    $this->range = $range; 

    return $this;
  }

  /**
   * Fetch spreadsheet values based on supplied range and spreadsheetId,
   * uses Sheets->spreadsheets_values->get
   * @chainable
   * 
   * @see https://developers.google.com/resources/api-libraries/documentation/sheets/v4/php/latest/class-Google_Service_Sheets_ValueRange.html
   * 
   * @param  array  $optParams - [majorDimension, valueRenderOption, 
   * dateTimeRenderOption]
   * @see majorDimension - https://developers.google.com/sheets/api/reference/rest/v4/Dimension
   * @see valueRenderOption - https://developers.google.com/sheets/api/reference/rest/v4/ValueRenderOption
   * @see dateTimeRenderOption - https://developers.google.com/sheets/api/reference/rest/v4/DateTimeRenderOption
   * 
   * @note adjusted default to valueRenderOption = UNFORMATTED_VALUE
   * 
   * @return  this  
   * 
   * @throws  SimplePHP\Exception\ThrownException  
   */
  public function fetchValues(
    array $optParams = [
      'valueRenderOption' => 'UNFORMATTED_VALUE' 
    ]
  ) {
    if (!$this->sheets) $this->initSheets();
    
    // handle spreadsheetId
    if (!$this->spreadsheetId)
      throw new ThrownException('Spreadsheet Id not defined');

    // handle range
    if (!$this->range)
      throw new ThrownException('Range is not defined');

    // get values from spreadsheet
    $this->data = ($this->sheets
      ->spreadsheets_values
      // @param String $spreadsheetId, String $range, Array $optParams
      ->get($this->spreadsheetId, $this->range, $optParams)
    )
    // [majorDimension, range, values]
    ->getValues()
    ;

    // above method will skip trailing empty values in the minor value, 
    // making the output array irregular, fix this by getting largest
    // count of minor value array
    $minorValueCount = (new SimpleArray($this->data))
    ->reduce(
      function($countAcc, $majorValue) {
        $count = count($majorValue);
        return ($count > $countAcc) ? $count : $countAcc;
      }, 0
    );

    // then inserting the missing with blank strings
    $this->data = (new SimpleArray($this->data))
    ->map(
      function($majorValue) use ($minorValueCount) {
        $count = count($majorValue);
        for (
          // arrays start with 0, whereas counts start from 1
          $i=($count-1); $i < ($minorValueCount-1); $i++
        ) {
          $majorValue = (new SimpleArray($majorValue))
          ->push('')
          ->get();
        }

        return $majorValue;
      }
    )
    ->get();

    return $this;
  }

  /**
   * Map headings (keys) to fetched data, transforms data into an object
   * @chainable
   * 
   * @param  array  $keysArr - [(string)heading1, heading2, ...]
   * 
   * @return  this  
   * 
   * @throws  SimplePHP\Exception\ThrownException  
   * 
   * @example  data before headings are mapped  
   * [
   *   (array) [ value1, value2, value3 ... ],
   *   ...
   * ]
   * 
   * @example  data after headings are mapped  
   * [
   *   (object) [
   *     heading1 => value1, 
   *     heading2 => value2, 
   *     heading3 => value3, 
   *     ...
   *   ],
   *   ...
   * ]
   */
  public function mapHeadings(Array $keysArr = []) {

    if (!$this->sheets) $this->initSheets();

    // handle $this->data
    if ((gettype($this->data) !== 'array') || (count($this->data) < 1))
      throw new ThrownException('Data is invalid');

    // handle keysArr
    // if keysArr is empty, assume first row of data are the keys
    // get first row from $this->data, set this as keysArr
    // remove first row from $this->data
    if (count($keysArr) < 1) {
      $keysArr = array_shift($this->data);
    }

    // map the data 
    $this->data = (new SimpleArray($this->data))
    ->map(
      function($majorValue) use ($keysArr) {
        $mappedOb = (object) [];
        (new SimpleArray($keysArr))
        ->forEach(
          function($key, $i) use (&$mappedOb, $majorValue) {
            $mappedOb->$key = $majorValue[$i];
          }
        );
        return $mappedOb;
      }
    )
    ->get();

    return $this;
  }

  /**
   * Getter, returns the contents of $this->data  
   * 
   * @return  array  
   */
  public function get() : array {
    return (array) $this->data;
  }
 }
?>