<?php
/**
 * Wrapper for the Google API
 * Handles Google_Client and oauth authentication
 * @author  Johnson Zhou (johnson@simplyuseful.io)
 * 
 * @class GoogleClient
 * 
 * @see https://github.com/googleapis/google-api-php-client
 * @see https://github.com/googleapis/google-api-php-client/blob/master/docs/oauth-web.md
 * 
 * Important: API scope and permissions
 * Client scopes must match this with enabled API scopes in Google API Console
 * @see $this->initClient
 * @see https://console.developers.google.com/
 * 
 */

namespace SimplePHP\SimpleIntegrations\SimpleGoogle;

use SimplePHP\SimpleData\SimpleFile; 
use SimplePHP\SimpleData\SimpleArray; 
use SimplePHP\Exception\OAuthRequired;

use \Google_Client;
use \Google_Service_Gmail;
use \Google_Service_Calendar;
use \Google_Service_Drive;
use \Google_Service_Sheets;

use \Google_Service_Exception;
use LogicException;

/**
 * @property  \Google_Client  client  
 * 
 * @method  public  initClient()  
 * @method  public  oauthCallback()  
 * @method  public  oauthReset()  
 * 
 * @method  protected  getServiceExceptionDetails()  
 */
class GoogleClient {

  /** @var  \Google_Client */
  protected $client;

  /** @var  string */
  private $clientName; 

  /** @var  array */
  private $token;
  // array [ access_token, refresh_token, expires_in, scope, token_type ]

  /** @var  string */
  private $tokenPath = '';

  /** @var  string */
  private $credentialPath = '';

  /** @var  string */
  private $redirectUri;

  /** @var  array */
  private $scopes = [];

  /**
   * @param  string  $clientName  name of the application  
   * @param  string  $credentialPath  path (file) to the google client credentials  
   * @param  string  $tokenPath  path (file) to save the token, ensure 'www-data' has read and write permissions  
   * @param  string  $redirectUri  
   * @param  array  $scopes  ['gmail', 'calendar', 'drive', 'sheets']
   */
  public function __construct(
    string  $clientName, 
    string  $credentialPath,
    string  $tokenPath,
    string  $redirectUri, 
    array  $scopes
  ) {
    $this->clientName = $clientName;
    $this->credentialPath = $credentialPath;
    $this->tokenPath = $tokenPath;
    $this->redirectUri = $redirectUri;
    $this->scopes = $scopes;
  }

  /**
   * Read token from file system
   * 
   * @return  bool  true if successfully read both tokens  
   */
  private function readToken() {
    try {
      // read the access token  
      $this->token = (new SimpleFile($this->tokenPath))
      ->read()
      ->jsonDecode(true);
      // array 

      return true;

    } catch (\Exception $e) {
      return false;
    }
  }

  /**
   * Saves the access and refresh_tokens to file system 
   * 
   * @return  bool  true if successfully wrote both tokens to file  
   */
  private function saveToken() {
    try {
      if (!($this->token)) return false;

      // write the access token
      (new SimpleFile($this->tokenPath))
      ->jsonEncode($this->token)
      ->write();

      return true;

    } catch (\Exception $e) {
      return false;
    }
  }

  /**
   * Deletes the token on file
   * 
   * @return  bool  true if successfully deleted both tokens  
   */
  private function deleteToken() {
    try {
      // delete the access token 
      (new SimpleFile($this->tokenPath))
      ->delete();

      return true;

    } catch (\Exception $e) {
      return false;
    }
  }

  /**
   * Saves the access token and refresh token after redirectURI callback
   * @see https://github.com/googleapis/google-api-php-client/blob/master/docs/oauth-web.md
   * 
   * @return  bool  $this->saveToken()
   */
  public function oauthCallback(string $code) {

    (!$this->client) && $this->initClient(true);
      
    // authenticate the code and get the access token
    $this->client->fetchAccessTokenWithAuthCode($code);
    $this->token = $this->client->getAccessToken();  // array 

    return $this->saveToken();
  }

  /**
   * Logs out of the oauth authentication,
   * by deleting the stored authentication tokens
   * 
   * @return  bool  $this->deleteToken()
   */
  public function oauthReset() {
    return $this->deleteToken();
  }

  /**
   * Gets the OAuth URL and passes it by throwing an OAuthRequired exception 
   * 
   * @throws  SimplePHP\Exception\OAuthRequired
   */
  private function throwOAuthRequired() {
    $authURL = $this->client->createAuthUrl();
    throw new OAuthRequired($authURL);
  }

  /**
   * Initialise Google_Client with common settings  
   * 1. Read credentials from file  
   * 2. Read tokens from file  
   * 3. If tokens expired, fetch new using refresh token  
   * 4. If authentication is required, will throw an instance of OAuthRequired  
   * 
   * @param  bool  $skipAuthentication  
   * 
   * @throws  SimplePHP\Exception\OAuthRequired
   * 
   * @example  scopes
   * Google_Service_Sheets::SPREADSHEETS  
   * Google_Service_Drive::DRIVE  
   * Google_Service_Gmail::MAIL_GOOGLE_COM  
   * Google_Service_Calendar::CALENDAR  
   */
  public function initClient(bool $skipAuthentication = false) {
    // reset state 
    $this->authenticated = false;
      
    // initialise Google_Client and set configuration
    $this->client = new Google_Client();
    $this->client->setApplicationName($this->clientName);
    $this->client->setAuthConfig(
      (new SimpleFile($this->credentialPath))->getPath()
    );

    (new SimpleArray($this->scopes))
    ->forEach(function($scope) {
      switch (strtolower($scope)) {
        case 'gmail':
          $this->client->addScope(Google_Service_Gmail::MAIL_GOOGLE_COM);
          break;

        case 'drive':
          $this->client->addScope(Google_Service_Drive::DRIVE);
          break;
          
        case 'calendar':
          $this->client->addScope(Google_Service_Calendar::CALENDAR);
          break;

        case 'sheets':
          $this->client->addScope(Google_Service_Sheets::SPREADSHEETS);
          break;

        default:
          break;
      }
    });
    
    $this->client->setRedirectUri($this->redirectUri); 

    // include a refresh_token 
    // approval prompt must be set to 'force' 
    $this->client->setAccessType('offline'); 
    $this->client->setApprovalPrompt('force');

    $this->client->setIncludeGrantedScopes(true);

    //?  $this->client->setState('testState');  
    //?  $this->client->setLoginHint('email@address.com');
    //?  $this->client->setApprovalPrompt('none|consent|select_account');

    if ($skipAuthentication) return;

    // read token file
    $read = $this->readToken();
    // bool  

    switch($read) {
      case true: 
        // set token on the client  
        $this->client->setAccessToken($this->token);

        // check token expiry  
        $expiredToken = $this->client->isAccessTokenExpired();

        // if expired, fetch new token with refresh token
        if ($expiredToken) {
          try {
            $this->token = $this->client
            ->fetchAccessTokenWithRefreshToken();

            // save the new token
            $this->saveToken();

          } catch (LogicException $e) {
            // unable to fetch and save the refresh token 
            // usually this is because refresh_token is not present 
            $this->throwOAuthRequired();
          }
        }

        break;

      case false: 
        $this->throwOAuthRequired();
        break;
    }
  }

  /**
   * Reads the Google_Service_Exception, which is a JSON object 
   * and returns the error message
   * 
   * @return  string  
   */
  protected function getServiceExceptionDetails(Google_Service_Exception $e) {
    try {
      return json_decode($e->getMessage())->error;
    } catch (\Exception $e) {
      return $e->getMessage();
    }
  }
}

?>