<?php
/**
 * Chainable Simple Gmail functions
 * @author  Johnson Zhou (johnson@simplyuseful.io)
 * 
 * @see https://developers.google.com/gmail/api/v1/reference
 */
namespace SimplePHP\SimpleIntegrations\SimpleGoogle;

use SimplePHP\SimpleIntegrations\SimpleGoogle\GoogleClient;
use SimplePHP\Exception\ThrownException;

use \Google_Service_Exception;
use \Google_Service_Gmail as Gmail;
use \Google_Service_Gmail_Message as Message;

/**
 * @method  protected  initGmail()  
 * @method  public  sendMessage()  
 */
class SimpleGmail extends GoogleClient {

  /** @var  Google_Service_Gmail */
  protected $gmail;

  /** 
   * @param  mixed  $params  params that will be passed to GoogleClient  
   * @see  SimplePHP\SimpleIntegrations\SimpleGoogle\GoogleClient  
   */
  public function __construct(...$params) {
    GoogleClient::__construct(...$params);
  }

  /**
   * Initialises the Gmail service
   * 
   * @throws  SimplePHP\Exception\OAuthRequired
   */
  protected function initGmail() {
    (!$this->client) && $this->initClient();
    $this->gmail = new Gmail($this->client);
  }

  /**
   * Sends a message via Gmail
   * Using GmailAPI->Users.messages->send
   * @param  string  $userId - the user's email address, 'me' = current user
   * @param  string  $emailRaw - the raw email encoded in base64url
   * 
   * @return  string  the sent message ID as returned by the GmailAPI
   * 
   * @throws  SimplePHP\Exception\ThrownException  
   * 
   * @example  usage  
   * SimpleGmail->send(userId, raw)  
   * Use SimpleEmail to create the raw message  
   * 
   * @see https://developers.google.com/gmail/api/v1/reference/users/messages/send
   */
  public function sendMessage(
    String $userId = 'me',
    String $emailRaw = null
  ) {
    try {
      
      $this->initGmail();

      // handle $emailRaw
      if (is_null($emailRaw))
      throw new ThrownException('Raw message is not provided', 400);

      // create the Message
      $message = new Message();
      $message->setRaw($emailRaw);

      // send the Message
      $send = $this->gmail
      ->users_messages
      ->send(
        $userId,
        $message
      );
      // MessageObject (Gmail.Users.messages)  
      // @see https://developers.google.com/gmail/api/v1/reference/users/messages#resource

      if (property_exists($send, 'id')) {
        return $send->id;
      } else {
        throw new ThrownException('Message not sent', 503);
      }
      
    } catch (Google_Service_Exception $e) {
      throw new ThrownException($this->getServiceExceptionDetails($e), 502);
    } 
  }
}
?>