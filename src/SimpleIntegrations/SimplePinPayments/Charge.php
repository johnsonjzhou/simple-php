<?php
/**
 * Wrapper for the Pin Payments Charges API
 * @author  Johnson Zhou (johnson@simplyuseful.io)
 * @see  https://pinpayments.com/developers/api-reference/charges
 * 
 * @method  public
 * createCharge
 */
namespace SimplePHP\SimpleIntegrations\SimplePinPayments;

use SimplePHP\SimpleIntegrations\SimplePinPayments;
use SimplePHP\Resource\PinPaymentsCharge;

/**
 * @method  createCharge  
 */
class Charge extends SimplePinPayments {

  /**
   * @param string $token — - the authorisation token for Pin Payments
   */
  public function __construct(String $token) {
    SimplePinPayments::__construct($token);
  }

  /**
   * Charges a card, card_token, or customer_token,
   * as set when creating a PinPaymentsCharge
   * POST /1/charges
   * @see  https://pinpayments.com/developers/api-reference/charges
   * 
   * @param  PinPaymentsCharge  $chargeObject
   * 
   * @return  object  [status, response ... ] || [status, error ...]
   */
  public function createCharge(PinPaymentsCharge $chargeObject) {
    try {
      if ($this->hasFault) return $this->hasFault;
      $response = $this->request(
        'POST', '/1/charges', [
          'auth' => [$this->token, null],
          'json' => $chargeObject
        ]
      );
      return $response;
    } catch (\Exception $e) {
      return $this->stop($e);
    }
  }
}
?>